import java.nio.ByteBuffer;

public class SimpleBuffer extends AbstractByteBuffer {

    ByteBuffer fieldA;


    @Override
    byte[] getBuffer(byte DUMMY) {
        try {
            byte[] var2 = new byte[this.fieldA.capacity()];
            this.fieldA.position(0);
            this.fieldA.get(var2);
            return var2;
        } catch (RuntimeException var3) {
            throw ClanMate.error(var3, "dj.a(" + ')');
        }
    }

    @Override
    void setBuffer(byte[] var1, byte DUMMY) {
        try {
            this.fieldA = ByteBuffer.allocateDirect(var1.length);
            this.fieldA.position(0);
            this.fieldA.put(var1);
        } catch (RuntimeException var3) {
            throw ClanMate.error(var3, "dj.v(" + ')');
        }
    }
}
