package com.tests;

/**
 * Project: OSRS(74)Rename
 * Date: 11-03-2015
 * Time: 20:00
 * Created by Dogerina.
 * Copyright under GPL license by Dogerina.
 */
public final class Queue<T extends DualNode> {

    DualNode<T> root = new DualNode<>();

    public Queue() {
        this.root.dualNext = this.root;
        this.root.dualPrev = this.root;
    }

    public void add(DualNode<T> var1) {
        if (var1.dualPrev != null) {
            var1.deleteDual();
        }
        var1.dualPrev = this.root.dualPrev;
        var1.dualNext = this.root;
        var1.dualPrev.dualNext = var1;
        var1.dualNext.dualPrev = var1;
    }

    public void insert(DualNode<T> var1) {
        if (var1.dualPrev != null) {
            var1.deleteDual();
        }
        var1.dualPrev = this.root;
        var1.dualNext = this.root.dualNext;
        var1.dualPrev.dualNext = var1;
        var1.dualNext.dualPrev = var1;
    }

    public DualNode<T> next() {
        DualNode<T> var1 = this.root.dualNext;
        return var1 == this.root ? null : var1;
    }

    void clear() {
        while (true) {
            DualNode<T> var1 = this.root.dualNext;
            if (var1 == this.root) {
                return;
            }
            var1.deleteDual();
        }
    }

    DualNode<T> pop() {
        DualNode<T> var1 = this.root.dualNext;
        if (var1 == this.root) {
            return null;
        } else {
            var1.deleteDual();
            return var1;
        }
    }
}
