package com.tests;

/**
 * Project: OSRS(74)Rename
 * Date: 11-03-2015
 * Time: 19:56
 * Created by Dogerina.
 * Copyright under GPL license by Dogerina.
 *
 * trying to figure out what the generics were..
 */
public class Node<T extends Node> {

    public long key;
    public Node<T> next;
    Node<T> prev;

    public void delete() {
        if (this.prev != null) {
            this.prev.next = this.next;
            this.next.prev = this.prev;
            this.next = null;
            this.prev = null;
        }
    }

    public boolean isLinked() {
        return this.prev != null;
    }
}
