package com.tests;

/**
 * Project: OSRS(74)Rename
 * Date: 11-03-2015
 * Time: 19:57
 * Created by Dogerina.
 * Copyright under GPL license by Dogerina.
 */
public class DualNode<T extends DualNode> extends Node<T> {

    public DualNode<T> dualNext;
    public DualNode<T> dualPrev;

    public void deleteDual() {
        if (this.dualPrev != null) {
            this.dualPrev.dualNext = this.dualNext;
            this.dualNext.dualPrev = this.dualPrev;
            this.dualNext = null;
            this.dualPrev = null;
        }
    }
}
