import java.awt.*;

public final class LandscapeMod extends Node {

    public static classBo fieldD;
    static ClanMate[] clanMates;
    static Sprite[] prayerHeadIcons;
    static int fieldFj;
    int spawnLife = 928555045;
    int objectStubType;
    int regionX;
    int regionY;
    int curObjectType;
    int prevObjectId;
    int prevObjectType;
    int curObjectId;
    int curObjectRot;
    int prevObjectRot;
    int spawnDelay = 0;
    int floor;

    static int method115(CharSequence var0, int var1, boolean var2, byte var3) {
        try {
            if (var1 >= 2 && var1 <= 36) {
                boolean var4 = false;
                boolean var5 = false;
                int var6 = 0;
                int var7 = var0.length();
                int var8 = 0;

                while (true) {
                    if (var8 >= var7) {
                        if (!var5) {
                            if (var3 <= 1) {
                                throw new IllegalStateException();
                            }

                            throw new NumberFormatException();
                        }

                        return var6;
                    }

                    if (var3 <= 1) {
                        throw new IllegalStateException();
                    }

                    label138:
                    {
                        char var9 = var0.charAt(var8);
                        if (0 == var8) {
                            if (45 == var9) {
                                if (var3 <= 1) {
                                    throw new IllegalStateException();
                                }

                                var4 = true;
                                break label138;
                            }

                            if (43 == var9) {
                                if (var3 <= 1) {
                                    throw new IllegalStateException();
                                }

                                if (var2) {
                                    if (var3 <= 1) {
                                        throw new IllegalStateException();
                                    }
                                    break label138;
                                }
                            }
                        }

                        int var12;
                        label139:
                        {
                            if (var9 >= 48) {
                                if (var3 <= 1) {
                                    throw new IllegalStateException();
                                }

                                if (var9 <= 57) {
                                    if (var3 <= 1) {
                                        throw new IllegalStateException();
                                    }

                                    var12 = var9 - 48;
                                    break label139;
                                }
                            }

                            if (var9 >= 65 && var9 <= 90) {
                                if (var3 <= 1) {
                                    throw new IllegalStateException();
                                }

                                var12 = var9 - 55;
                            } else {
                                if (var9 < 97) {
                                    break;
                                }

                                if (var3 <= 1) {
                                    throw new IllegalStateException();
                                }

                                if (var9 > 122) {
                                    break;
                                }

                                if (var3 <= 1) {
                                    throw new IllegalStateException();
                                }

                                var12 = var9 - 87;
                            }
                        }

                        if (var12 >= var1) {
                            if (var3 <= 1) {
                                throw new IllegalStateException();
                            }

                            throw new NumberFormatException();
                        }

                        if (var4) {
                            var12 = -var12;
                        }

                        int var10 = var12 + var6 * var1;
                        if (var10 / var1 != var6) {
                            if (var3 <= 1) {
                                throw new IllegalStateException();
                            }

                            throw new NumberFormatException();
                        }

                        var6 = var10;
                        var5 = true;
                    }

                    ++var8;
                }

                throw new NumberFormatException();
            } else {
                throw new IllegalArgumentException("");
            }
        } catch (RuntimeException var11) {
            throw ClanMate.error(var11, "o.l(" + ')');
        }
    }

    static void method116(int var0, short var1) {
        try {
            if (var0 == -3) {
                if (var1 <= 2047) {
                    return;
                }

                classX.method200(StringConstants.fieldAt, StringConstants.fieldAw, StringConstants.fieldAo, (byte) -4);
            } else if (var0 == -2) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldAg, StringConstants.fieldAd, StringConstants.fieldAu, (byte) -45);
            } else if (var0 == -1) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldAk, StringConstants.fieldAj, StringConstants.fieldAf, (byte) -91);
            } else if (var0 == 3) {
                classX.method200(StringConstants.fieldAb, StringConstants.fieldAc, StringConstants.fieldAp, (byte) -4);
            } else if (4 == var0) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldAy, StringConstants.fieldBu, StringConstants.fieldBo, (byte) -26);
            } else if (var0 == 5) {
                if (var1 <= 2047) {
                    return;
                }

                classX.method200(StringConstants.fieldBi, StringConstants.fieldBs, StringConstants.fieldBl, (byte) -41);
            } else if (var0 == 6) {
                if (var1 <= 2047) {
                    return;
                }

                classX.method200(StringConstants.fieldBp, StringConstants.fieldBw, StringConstants.fieldBm, (byte) 108);
            } else if (7 == var0) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldBz, StringConstants.fieldBr, StringConstants.fieldBe, (byte) 48);
            } else if (8 == var0) {
                if (var1 <= 2047) {
                    return;
                }

                classX.method200(StringConstants.fieldBb, StringConstants.fieldBc, StringConstants.fieldBk, (byte) 35);
            } else if (9 == var0) {
                classX.method200(StringConstants.fieldBf, StringConstants.fieldBg, StringConstants.fieldBt, (byte) -17);
            } else if (10 == var0) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldBx, StringConstants.fieldBn, StringConstants.fieldBq, (byte) 95);
            } else if (var0 == 11) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldBa, StringConstants.fieldBv, StringConstants.fieldBd, (byte) -18);
            } else if (12 == var0) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldBh, StringConstants.fieldBj, StringConstants.fieldBy, (byte) 11);
            } else if (var0 == 13) {
                classX.method200(StringConstants.fieldCr, StringConstants.fieldCt, StringConstants.fieldCw, (byte) -62);
            } else if (var0 == 14) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldCq, StringConstants.fieldCi, StringConstants.fieldCf, (byte) -29);
            } else if (16 == var0) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldCo, StringConstants.fieldCj, StringConstants.fieldCd, (byte) 59);
            } else if (var0 == 17) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldCb, StringConstants.fieldCh, StringConstants.fieldCn, (byte) -7);
            } else if (18 == var0) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldCp, StringConstants.fieldCk, StringConstants.fieldCg, (byte) 13);
            } else if (19 == var0) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldCy, StringConstants.fieldCv, StringConstants.fieldCc, (byte) -36);
            } else if (20 == var0) {
                classX.method200(StringConstants.fieldCx, StringConstants.fieldCe, StringConstants.fieldCl, (byte) -109);
            } else if (var0 == 22) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldCz, StringConstants.fieldCm, StringConstants.fieldCu, (byte) 98);
            } else if (23 == var0) {
                classX.method200(StringConstants.fieldCa, StringConstants.fieldCs, StringConstants.fieldDg, (byte) -74);
            } else if (var0 == 24) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldDd, StringConstants.fieldDp, StringConstants.fieldDu, (byte) -7);
            } else if (25 == var0) {
                if (var1 <= 2047) {
                    return;
                }

                classX.method200(StringConstants.fieldDj, StringConstants.fieldDi, StringConstants.fieldDo, (byte) -52);
            } else if (26 == var0) {
                classX.method200(StringConstants.fieldDq, StringConstants.fieldDh, StringConstants.fieldDf, (byte) 114);
            } else if (var0 == 27) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldDw, StringConstants.fieldDk, StringConstants.fieldDr, (byte) 21);
            } else if (var0 == 31) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldDa, StringConstants.fieldDz, StringConstants.fieldDy, (byte) -45);
            } else if (32 == var0) {
                classX.method200(StringConstants.fieldDb, StringConstants.fieldDx, StringConstants.fieldDe, (byte) -45);
            } else if (37 == var0) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldDm, StringConstants.fieldEt, StringConstants.fieldEp, (byte) 38);
            } else if (var0 == 38) {
                if (var1 <= 2047) {
                    throw new IllegalStateException();
                }

                classX.method200(StringConstants.fieldEj, StringConstants.fieldEe, StringConstants.fieldEq, (byte) 14);
            } else if (55 == var0) {
                if (var1 <= 2047) {
                    return;
                }

                classX.method200(StringConstants.fieldEo, StringConstants.fieldEi, StringConstants.fieldEc, (byte) 63);
            } else {
                if (var0 == 56) {
                    classX.method200(StringConstants.fieldEh, StringConstants.fieldEl, StringConstants.fieldEb, (byte) -32);
                    UnknownEnum.method373(11, (byte) -28);
                    return;
                }

                if (57 == var0) {
                    if (var1 <= 2047) {
                        throw new IllegalStateException();
                    }

                    classX.method200(StringConstants.fieldEy, StringConstants.fieldEg, StringConstants.fieldEm, (byte) 29);
                    UnknownEnum.method373(11, (byte) -48);
                    return;
                }

                classX.method200(StringConstants.fieldEa, StringConstants.fieldEf, StringConstants.fieldEx, (byte) 59);
            }

            UnknownEnum.method373(10, (byte) -2);
        } catch (RuntimeException var2) {
            throw ClanMate.error(var2, "o.t(" + ')');
        }
    }

    static final void method117(int DUMMY) {
        try {
            int var1;
            int var2;
            int var3;
            int var4;
            GroundItem var31;
            if (Client.packetId * -5466435 == 17) {
                var1 = Client.packet.readUByte(-243614721);
                var2 = classJ.fieldDc * 968617591 + (var1 >> 4 & 7);
                var3 = (var1 & 7) + UtilClass14.fieldDn * 1626548707;
                var4 = Client.packet.ap(-1847057619);
                if (var2 >= 0) {
                    if (DUMMY <= 399667269) {
                        throw new IllegalStateException();
                    }

                    if (var3 >= 0) {
                        if (DUMMY <= 399667269) {
                            throw new IllegalStateException();
                        }

                        if (var2 < 104) {
                            if (DUMMY <= 399667269) {
                                throw new IllegalStateException();
                            }

                            if (var3 < 104) {
                                if (DUMMY <= 399667269) {
                                    return;
                                }

                                Deque<GroundItem> var34 = Client.groundItemDeques[ClanMate.floorLevel * 87713183][var2][var3];
                                if (var34 != null) {
                                    if (DUMMY <= 399667269) {
                                        throw new IllegalStateException();
                                    }

                                    for (var31 = var34.next(); var31 != null; var31 = var34.n()) {
                                        if (DUMMY <= 399667269) {
                                            throw new IllegalStateException();
                                        }

                                        if (var31.itemId * 2057899465 == (var4 & 32767)) {
                                            if (DUMMY <= 399667269) {
                                                throw new IllegalStateException();
                                            }

                                            var31.delete();
                                            break;
                                        }
                                    }

                                    if (var34.next() == null) {
                                        Client.groundItemDeques[ClanMate.floorLevel * 87713183][var2][var3] = null;
                                    }

                                    WidgetNode.method97(var2, var3, (byte) -2);
                                }
                            }
                        }
                    }
                }

            } else {
                int var5;
                int var6;
                if (Client.packetId * -5466435 == 150) {
                    if (DUMMY <= 399667269) {
                        throw new IllegalStateException();
                    } else {
                        var1 = Client.packet.readUByte(203803592);
                        var2 = (var1 >> 4 & 7) + classJ.fieldDc * 968617591;
                        var3 = (var1 & 7) + UtilClass14.fieldDn * 1626548707;
                        var4 = Client.packet.readUShort(829603351);
                        var5 = Client.packet.readUShort(829603351);
                        var6 = Client.packet.readUShort(829603351);
                        if (var2 >= 0) {
                            if (DUMMY <= 399667269) {
                                throw new IllegalStateException();
                            }

                            if (var3 >= 0) {
                                if (DUMMY <= 399667269) {
                                    throw new IllegalStateException();
                                }

                                if (var2 < 104) {
                                    if (DUMMY <= 399667269) {
                                        return;
                                    }

                                    if (var3 < 104) {
                                        Deque<GroundItem> var35 = Client.groundItemDeques[ClanMate.floorLevel * 87713183][var2][var3];
                                        if (var35 != null) {
                                            for (GroundItem var37 = var35.next(); var37 != null; var37 = var35.n()) {
                                                if (DUMMY <= 399667269) {
                                                    return;
                                                }

                                                if ((var4 & 32767) == var37.itemId * 2057899465) {
                                                    if (DUMMY <= 399667269) {
                                                        return;
                                                    }

                                                    if (var5 == var37.quantity * 1902127977) {
                                                        var37.quantity = -243329831 * var6;
                                                        break;
                                                    }
                                                }
                                            }

                                            WidgetNode.method97(var2, var3, (byte) -2);
                                        }
                                    }
                                }
                            }
                        }

                    }
                } else {
                    int var7;
                    int var8;
                    int var9;
                    if (Client.packetId * -5466435 == 134) {
                        if (DUMMY <= 399667269) {
                            throw new IllegalStateException();
                        }

                        var1 = Client.packet.readUByte(-1636796239);
                        var2 = (var1 >> 4 & 7) + classJ.fieldDc * 968617591;
                        var3 = (var1 & 7) + UtilClass14.fieldDn * 1626548707;
                        var4 = Client.packet.readUShort(829603351);
                        var5 = Client.packet.readUByte(63922298);
                        var6 = var5 >> 4 & 15;
                        var7 = var5 & 7;
                        var8 = Client.packet.readUByte(526898649);
                        if (var2 >= 0 && var3 >= 0) {
                            if (DUMMY <= 399667269) {
                                return;
                            }

                            if (var2 < 104) {
                                if (DUMMY <= 399667269) {
                                    throw new IllegalStateException();
                                }

                                if (var3 < 104) {
                                    if (DUMMY <= 399667269) {
                                        throw new IllegalStateException();
                                    }

                                    var9 = var6 + 1;
                                    if (Renderable.localPlayer.queueX[0] >= var2 - var9) {
                                        if (DUMMY <= 399667269) {
                                            throw new IllegalStateException();
                                        }

                                        if (Renderable.localPlayer.queueX[0] <= var9 + var2) {
                                            if (DUMMY <= 399667269) {
                                                throw new IllegalStateException();
                                            }

                                            if (Renderable.localPlayer.queueY[0] >= var3 - var9) {
                                                if (DUMMY <= 399667269) {
                                                    return;
                                                }

                                                if (Renderable.localPlayer.queueY[0] <= var9 + var3) {
                                                    if (DUMMY <= 399667269) {
                                                        return;
                                                    }

                                                    if (0 != Client.fieldNm * -1976627615) {
                                                        if (DUMMY <= 399667269) {
                                                            throw new IllegalStateException();
                                                        }

                                                        if (var7 > 0) {
                                                            if (DUMMY <= 399667269) {
                                                                return;
                                                            }

                                                            if (Client.fieldNx * 1993078547 < 50) {
                                                                if (DUMMY <= 399667269) {
                                                                    throw new IllegalStateException();
                                                                }

                                                                Client.fieldNp[Client.fieldNx * 1993078547] = var4;
                                                                Client.fieldNl[Client.fieldNx * 1993078547] = var7;
                                                                Client.fieldNw[Client.fieldNx * 1993078547] = var8;
                                                                Client.fieldNq[Client.fieldNx * 1993078547] = null;
                                                                Client.fieldNj[Client.fieldNx * 1993078547] = var6 + (var2 << 16) + (var3 << 8);
                                                                Client.fieldNx += 1441295131;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (Client.packetId * -5466435 == 148) {
                        if (DUMMY <= 399667269) {
                            throw new IllegalStateException();
                        } else {
                            var1 = Client.packet.readUShort(829603351);
                            var2 = Client.packet.readInvertedUByte((byte) 0);
                            var3 = classJ.fieldDc * 968617591 + (var2 >> 4 & 7);
                            var4 = UtilClass14.fieldDn * 1626548707 + (var2 & 7);
                            var5 = Client.packet.readUByte(-324150413);
                            var6 = var5 >> 2;
                            var7 = var5 & 3;
                            var8 = Client.objectType2StubType[var6];
                            if (var3 >= 0) {
                                if (DUMMY <= 399667269) {
                                    throw new IllegalStateException();
                                }

                                if (var4 >= 0 && var3 < 104) {
                                    if (DUMMY <= 399667269) {
                                        throw new IllegalStateException();
                                    }

                                    if (var4 < 104) {
                                        Projectile.spawnObjectLater(ClanMate.floorLevel * 87713183, var3, var4, var8, var1, var6, var7, 0, -1, (byte) 11);
                                    }
                                }
                            }

                        }
                    } else if (32 == Client.packetId * -5466435) {
                        if (DUMMY <= 399667269) {
                            throw new IllegalStateException();
                        } else {
                            var1 = Client.packet.readUByte(-1260794142);
                            var2 = classJ.fieldDc * 968617591 + (var1 >> 4 & 7);
                            var3 = (var1 & 7) + UtilClass14.fieldDn * 1626548707;
                            var4 = Client.packet.readUShort(829603351);
                            var5 = Client.packet.readUByte(-795865261);
                            var6 = Client.packet.readUShort(829603351);
                            if (var2 >= 0) {
                                if (DUMMY <= 399667269) {
                                    throw new IllegalStateException();
                                }

                                if (var3 >= 0) {
                                    if (DUMMY <= 399667269) {
                                        return;
                                    }

                                    if (var2 < 104) {
                                        if (DUMMY <= 399667269) {
                                            return;
                                        }

                                        if (var3 < 104) {
                                            if (DUMMY <= 399667269) {
                                                throw new IllegalStateException();
                                            }

                                            var2 = 64 + 128 * var2;
                                            var3 = 128 * var3 + 64;
                                            GraphicsStub var33 = new GraphicsStub(var4, ClanMate.floorLevel * 87713183, var2, var3, ItemPile.getTileHeight(var2, var3, ClanMate.floorLevel * 87713183, 1088377195) - var5, var6, Client.engineCycle * -1223223371);
                                            Client.fieldHa.add(var33);
                                        }
                                    }
                                }
                            }

                        }
                    } else if (Client.packetId * -5466435 == 206) {
                        if (DUMMY <= 399667269) {
                            throw new IllegalStateException();
                        } else {
                            var1 = Client.packet.readInvertedUByte((byte) 0);
                            var2 = var1 >> 2;
                            var3 = var1 & 3;
                            var4 = Client.objectType2StubType[var2];
                            var5 = Client.packet.aw((byte) 7);
                            var6 = classJ.fieldDc * 968617591 + (var5 >> 4 & 7);
                            var7 = UtilClass14.fieldDn * 1626548707 + (var5 & 7);
                            if (var6 >= 0) {
                                if (DUMMY <= 399667269) {
                                    throw new IllegalStateException();
                                }

                                if (var7 >= 0 && var6 < 104) {
                                    if (DUMMY <= 399667269) {
                                        throw new IllegalStateException();
                                    }

                                    if (var7 < 104) {
                                        if (DUMMY <= 399667269) {
                                            throw new IllegalStateException();
                                        }

                                        Projectile.spawnObjectLater(ClanMate.floorLevel * 87713183, var6, var7, var4, -1, var2, var3, 0, -1, (byte) 6);
                                    }
                                }
                            }

                        }
                    } else {
                        int var10;
                        if (Client.packetId * -5466435 == 186) {
                            var1 = Client.packet.readInvertedUByte((byte) 0);
                            var2 = classJ.fieldDc * 968617591 + (var1 >> 4 & 7);
                            var3 = (var1 & 7) + UtilClass14.fieldDn * 1626548707;
                            var4 = Client.packet.ao((byte) 86);
                            var5 = var4 >> 2;
                            var6 = var4 & 3;
                            var7 = Client.objectType2StubType[var5];
                            var8 = Client.packet.ac((byte) 17);
                            if (var2 >= 0) {
                                if (DUMMY <= 399667269) {
                                    throw new IllegalStateException();
                                }

                                if (var3 >= 0) {
                                    if (DUMMY <= 399667269) {
                                        return;
                                    }

                                    if (var2 < 103) {
                                        if (DUMMY <= 399667269) {
                                            throw new IllegalStateException();
                                        }

                                        if (var3 < 103) {
                                            if (DUMMY <= 399667269) {
                                                throw new IllegalStateException();
                                            }

                                            if (0 == var7) {
                                                Boundary var38 = UtilClass28.landscape.getBoundaryAt(ClanMate.floorLevel * 87713183, var2, var3);
                                                if (var38 != null) {
                                                    if (DUMMY <= 399667269) {
                                                        throw new IllegalStateException();
                                                    }

                                                    var10 = var38.uid * -1483668077 >> 14 & 32767;
                                                    if (2 == var5) {
                                                        if (DUMMY <= 399667269) {
                                                            throw new IllegalStateException();
                                                        }

                                                        var38.fieldM = new DynamicObject(var10, 2, var6 + 4, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var38.fieldM);
                                                        var38.fieldW = new DynamicObject(var10, 2, 1 + var6 & 3, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var38.fieldW);
                                                    } else {
                                                        var38.fieldM = new DynamicObject(var10, var5, var6, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var38.fieldM);
                                                    }
                                                }
                                            }

                                            if (var7 == 1) {
                                                if (DUMMY <= 399667269) {
                                                    throw new IllegalStateException();
                                                }

                                                BoundaryDecoration var39 = UtilClass28.landscape.getBoundaryDecoration(ClanMate.floorLevel * 87713183, var2, var3);
                                                if (null != var39) {
                                                    label621:
                                                    {
                                                        var10 = var39.uid * -1933340315 >> 14 & 32767;
                                                        if (4 != var5) {
                                                            if (DUMMY <= 399667269) {
                                                                return;
                                                            }

                                                            if (5 != var5) {
                                                                if (var5 == 6) {
                                                                    if (DUMMY <= 399667269) {
                                                                        return;
                                                                    }

                                                                    var39.fieldE = new DynamicObject(var10, 4, var6 + 4, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var39.fieldE);
                                                                } else if (7 == var5) {
                                                                    if (DUMMY <= 399667269) {
                                                                        throw new IllegalStateException();
                                                                    }

                                                                    var39.fieldE = new DynamicObject(var10, 4, (2 + var6 & 3) + 4, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var39.fieldE);
                                                                } else if (var5 == 8) {
                                                                    if (DUMMY <= 399667269) {
                                                                        throw new IllegalStateException();
                                                                    }

                                                                    var39.fieldE = new DynamicObject(var10, 4, 4 + var6, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var39.fieldE);
                                                                    var39.fieldN = new DynamicObject(var10, 4, (var6 + 2 & 3) + 4, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var39.fieldN);
                                                                }
                                                                break label621;
                                                            }

                                                            if (DUMMY <= 399667269) {
                                                                throw new IllegalStateException();
                                                            }
                                                        }

                                                        var39.fieldE = new DynamicObject(var10, 4, var6, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var39.fieldE);
                                                    }
                                                }
                                            }

                                            if (2 == var7) {
                                                EntityMarker var40 = UtilClass28.landscape.getObjectAt(ClanMate.floorLevel * 87713183, var2, var3);
                                                if (11 == var5) {
                                                    var5 = 10;
                                                }

                                                if (null != var40) {
                                                    var40.entity = new DynamicObject(var40.uid * -1211010875 >> 14 & 32767, var5, var6, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var40.entity);
                                                }
                                            }

                                            if (var7 == 3) {
                                                TileDecoration var41 = UtilClass28.landscape.c(ClanMate.floorLevel * 87713183, var2, var3);
                                                if (null != var41) {
                                                    var41.deco = new DynamicObject(var41.uid * 2119625601 >> 14 & 32767, 22, var6, ClanMate.floorLevel * 87713183, var2, var3, var8, false, var41.deco);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        } else if (218 == Client.packetId * -5466435) {
                            var1 = Client.packet.readUShort(829603351);
                            var2 = Client.packet.ao((byte) 5);
                            var3 = classJ.fieldDc * 968617591 + (var2 >> 4 & 7);
                            var4 = UtilClass14.fieldDn * 1626548707 + (var2 & 7);
                            var5 = Client.packet.ab(1774505017);
                            if (var3 >= 0) {
                                if (DUMMY <= 399667269) {
                                    throw new IllegalStateException();
                                }

                                if (var4 >= 0) {
                                    if (DUMMY <= 399667269) {
                                        throw new IllegalStateException();
                                    }

                                    if (var3 < 104) {
                                        if (DUMMY <= 399667269) {
                                            throw new IllegalStateException();
                                        }

                                        if (var4 < 104) {
                                            if (DUMMY <= 399667269) {
                                                throw new IllegalStateException();
                                            }

                                            var31 = new GroundItem();
                                            var31.itemId = 1159896185 * var1;
                                            var31.quantity = -243329831 * var5;
                                            if (Client.groundItemDeques[ClanMate.floorLevel * 87713183][var3][var4] == null) {
                                                if (DUMMY <= 399667269) {
                                                    throw new IllegalStateException();
                                                }

                                                Client.groundItemDeques[ClanMate.floorLevel * 87713183][var3][var4] = new Deque<>();
                                            }

                                            Client.groundItemDeques[ClanMate.floorLevel * 87713183][var3][var4].add(var31);
                                            WidgetNode.method97(var3, var4, (byte) -2);
                                        }
                                    }
                                }
                            }

                        } else {
                            int var11;
                            int var12;
                            if (Client.packetId * -5466435 == 180) {
                                if (DUMMY > 399667269) {
                                    var1 = Client.packet.readUByte(-779138393);
                                    var2 = (var1 >> 4 & 7) + classJ.fieldDc * 968617591;
                                    var3 = UtilClass14.fieldDn * 1626548707 + (var1 & 7);
                                    var4 = var2 + Client.packet.readByte(250344736);
                                    var5 = var3 + Client.packet.readByte(250344736);
                                    var6 = Client.packet.j((byte) 1);
                                    var7 = Client.packet.readUShort(829603351);
                                    var8 = Client.packet.readUByte(-359078542) * 4;
                                    var9 = Client.packet.readUByte(538088424) * 4;
                                    var10 = Client.packet.readUShort(829603351);
                                    var11 = Client.packet.readUShort(829603351);
                                    var12 = Client.packet.readUByte(265788640);
                                    int var42 = Client.packet.readUByte(-685542637);
                                    if (var2 >= 0) {
                                        if (DUMMY <= 399667269) {
                                            return;
                                        }

                                        if (var3 >= 0) {
                                            if (DUMMY <= 399667269) {
                                                return;
                                            }

                                            if (var2 < 104) {
                                                if (DUMMY <= 399667269) {
                                                    throw new IllegalStateException();
                                                }

                                                if (var3 < 104) {
                                                    if (DUMMY <= 399667269) {
                                                        throw new IllegalStateException();
                                                    }

                                                    if (var4 >= 0) {
                                                        if (DUMMY <= 399667269) {
                                                            return;
                                                        }

                                                        if (var5 >= 0) {
                                                            if (DUMMY <= 399667269) {
                                                                return;
                                                            }

                                                            if (var4 < 104 && var5 < 104 && '\uffff' != var7) {
                                                                if (DUMMY <= 399667269) {
                                                                    throw new IllegalStateException();
                                                                }

                                                                var2 = 128 * var2 + 64;
                                                                var3 = 64 + 128 * var3;
                                                                var4 = 128 * var4 + 64;
                                                                var5 = var5 * 128 + 64;
                                                                Projectile var43 = new Projectile(var7, ClanMate.floorLevel * 87713183, var2, var3, ItemPile.getTileHeight(var2, var3, ClanMate.floorLevel * 87713183, 1822408952) - var8, var10 + Client.engineCycle * -1223223371, var11 + Client.engineCycle * -1223223371, var12, var42, var6, var9);
                                                                var43.a(var4, var5, ItemPile.getTileHeight(var4, var5, ClanMate.floorLevel * 87713183, 1176375973) - var9, Client.engineCycle * -1223223371 + var10, 1303475738);
                                                                Client.fieldHn.add(var43);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            } else {
                                if (Client.packetId * -5466435 == 67) {

                                    var1 = Client.packet.ap(-1797152954);
                                    var2 = Client.packet.ab(2124647261);
                                    var3 = Client.packet.ab(868165550);
                                    var4 = Client.packet.ao((byte) 47);
                                    var5 = var4 >> 2;
                                    var6 = var4 & 3;
                                    var7 = Client.objectType2StubType[var5];
                                    byte var32 = Client.packet.readByte(250344736);
                                    byte var36 = Client.packet.readByte(250344736);
                                    var10 = Client.packet.readInvertedUByte((byte) 0);
                                    var11 = (var10 >> 4 & 7) + classJ.fieldDc * 968617591;
                                    var12 = UtilClass14.fieldDn * 1626548707 + (var10 & 7);
                                    byte var13 = Client.packet.ad(209080471);
                                    byte var14 = Client.packet.readInvertedByte((byte) 58);
                                    int var15 = Client.packet.ac((byte) 74);
                                    Player var16;
                                    if (Client.playerServerIndex * -1508358289 == var3) {
                                        var16 = Renderable.localPlayer;
                                    } else {
                                        var16 = Client.players[var3];
                                    }

                                    if (var16 != null) {
                                        ObjectDefinition var17;
                                        int var18;
                                        int var19;
                                        label644:
                                        {
                                            var17 = PlayerConfig.getObjectDef(var1, 885530217);
                                            if (var6 != 1) {
                                                if (DUMMY <= 399667269) {
                                                    return;
                                                }

                                                if (var6 != 3) {
                                                    var18 = var17.sizeX * -1151280759;
                                                    var19 = var17.sizeY * -1255940743;
                                                    break label644;
                                                }

                                                if (DUMMY <= 399667269) {
                                                    throw new IllegalStateException();
                                                }
                                            }

                                            var18 = var17.sizeY * -1255940743;
                                            var19 = var17.sizeX * -1151280759;
                                        }

                                        int var20 = var11 + (var18 >> 1);
                                        int var21 = (1 + var18 >> 1) + var11;
                                        int var22 = var12 + (var19 >> 1);
                                        int var23 = var12 + (1 + var19 >> 1);
                                        int[][] var24 = classM.tileHeights[ClanMate.floorLevel * 87713183];
                                        int var25 = var24[var21][var23] + var24[var20][var22] + var24[var21][var22] + var24[var20][var23] >> 2;
                                        int var26 = (var18 << 6) + (var11 << 7);
                                        int var27 = (var19 << 6) + (var12 << 7);
                                        Model var28 = var17.n(var5, var6, var24, var26, var25, var27, (byte) 16);
                                        if (null != var28) {
                                            if (DUMMY <= 399667269) {
                                                throw new IllegalStateException();
                                            }

                                            Projectile.spawnObjectLater(ClanMate.floorLevel * 87713183, var11, var12, var7, -1, 0, 0, var2 + 1, var15 + 1, (byte) 87);
                                            var16.objTransformStartCycle = (var2 + Client.engineCycle * -1223223371) * -136876771;
                                            var16.objTransformEndCycle = (var15 + Client.engineCycle * -1223223371) * 1691781299;
                                            var16.transformObjModel = var28;
                                            var16.objectLocX = 461127616 * var18 + var11 * 922255232;
                                            var16.objectLocY = var19 * 252354240 + var12 * 504708480;
                                            var16.objectLocZ = -1823363617 * var25;
                                            byte var29;

                                            if (var14 > var13) {
                                                var29 = var14;
                                                var14 = var13;
                                                var13 = var29;
                                            }

                                            if (var36 > var32) {
                                                var29 = var36;
                                                var36 = var32;
                                                var32 = var29;
                                            }

                                            var16.objRegionX = (var11 + var14) * -1626034921;
                                            var16.objMaxRegionX = -1205480525 * (var11 + var13);
                                            var16.objRegionY = (var36 + var12) * -303997603;
                                            var16.objMaxRegionY = 394406681 * (var12 + var32);
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        } catch (RuntimeException var30) {
            throw ClanMate.error(var30, "o.ac(" + ')');
        }
    }

    public static classGm method118(ReferenceTable var0, ReferenceTable var1, String var2, String var3, int var4) {
        try {
            int var5 = var0.h(var2, 921294597);
            int var6 = var0.u(var5, var3, 633939902);
            return classAt.method368(var0, var1, var5, var6, -1659684103);
        } catch (RuntimeException var7) {
            throw ClanMate.error(var7, "o.n(" + ')');
        }
    }

    public static final classAp method119(TaskHandler var0, Component var1, int var2, int var3, byte var4) {
        try {
            if (classAp.audioSampleRate * 2098856905 == 0) {
                if (var4 != 10) {
                    throw new IllegalStateException();
                } else {
                    throw new IllegalStateException();
                }
            } else {
                if (var2 >= 0) {
                    if (var4 != 10) {
                        throw new IllegalStateException();
                    }

                    if (var2 < 2) {
                        if (var3 < 256) {
                            var3 = 256;
                        }

                        try {
                            classBb var9 = new classBb();
                            byte var10001;
                            if (classAp.twoChannels) {
                                if (var4 != 10) {
                                    throw new IllegalStateException();
                                }

                                var10001 = 2;
                            } else {
                                var10001 = 1;
                            }

                            var9.fieldN = new int[var10001 * 256];
                            var9.fieldX = var3 * 833212989;
                            var9.a(var1);
                            var9.fieldD = ((var3 & -1024) + 1024) * -312184257;
                            if (var9.fieldD * 205159871 > 16384) {
                                if (var4 != 10) {
                                    throw new IllegalStateException();
                                }

                                var9.fieldD = 479182848;
                            }

                            var9.v(var9.fieldD * 205159871);
                            if (classAp.fieldL * 175727203 > 0) {
                                if (var4 != 10) {
                                    throw new IllegalStateException();
                                }

                                if (classAp.fieldM == null) {
                                    if (var4 != 10) {
                                        throw new IllegalStateException();
                                    }

                                    classAp.fieldM = new classBf();
                                    classAp.fieldM.fieldA = var0;
                                    var0.getThreadTask(classAp.fieldM, classAp.fieldL * 175727203, 1409603907);
                                }
                            }

                            if (null != classAp.fieldM) {
                                if (var4 != 10) {
                                    throw new IllegalStateException();
                                }

                                if (null != classAp.fieldM.fieldV[var2]) {
                                    throw new IllegalArgumentException();
                                }

                                classAp.fieldM.fieldV[var2] = var9;
                            }

                            return var9;
                        } catch (Throwable var7) {
                            try {
                                classAb var5 = new classAb(var0, var2);
                                var5.fieldN = new int[256 * (classAp.twoChannels ? 2 : 1)];
                                var5.fieldX = 833212989 * var3;
                                var5.a(var1);
                                var5.fieldD = 479182848;
                                var5.v(var5.fieldD * 205159871);
                                if (classAp.fieldL * 175727203 > 0 && classAp.fieldM == null) {
                                    classAp.fieldM = new classBf();
                                    classAp.fieldM.fieldA = var0;
                                    var0.getThreadTask(classAp.fieldM, classAp.fieldL * 175727203, 624612212);
                                }

                                if (classAp.fieldM != null) {
                                    if (var4 != 10) {
                                        throw new IllegalStateException();
                                    }

                                    if (classAp.fieldM.fieldV[var2] != null) {
                                        if (var4 != 10) {
                                            throw new IllegalStateException();
                                        }

                                        throw new IllegalArgumentException();
                                    }

                                    classAp.fieldM.fieldV[var2] = var5;
                                }

                                return var5;
                            } catch (Throwable var6) {
                                return new classAp();
                            }
                        }
                    }

                    if (var4 != 10) {
                        throw new IllegalStateException();
                    }
                }

                throw new IllegalArgumentException();
            }
        } catch (RuntimeException var8) {
            throw ClanMate.error(var8, "o.y(" + ')');
        }
    }

    static final void method120(Widget var0, int var1, int var2, byte var3) {
        try {
            if (1 == var0.fieldQ * 1240781149) {
                ItemPile.insertAction(var0.fieldDk, "", 24, 0, 0, var0.id * -1866039137, (byte) -8);
            }

            String var4;
            if (var0.fieldQ * 1240781149 == 2) {
                if (var3 != 3) {
                    throw new IllegalStateException();
                }

                if (!Client.isSpellSelected) {
                    if (var3 != 3) {
                        return;
                    }

                    var4 = classCs.method304(var0, 469151008);
                    if (null != var4) {
                        if (var3 != 3) {
                            return;
                        }

                        ItemPile.insertAction(var4, Buffer.method67('\uff00', 1640266589) + var0.fieldDw, 25, 0, -1, var0.id * -1866039137, (byte) -128);
                    }
                }
            }

            if (3 == var0.fieldQ * 1240781149) {
                if (var3 != 3) {
                    throw new IllegalStateException();
                }

                ItemPile.insertAction(StringConstants.CLOSE, "", 26, 0, 0, var0.id * -1866039137, (byte) -62);
            }

            if (4 == var0.fieldQ * 1240781149) {
                if (var3 != 3) {
                    throw new IllegalStateException();
                }

                ItemPile.insertAction(var0.fieldDk, "", 28, 0, 0, var0.id * -1866039137, (byte) -30);
            }

            if (5 == var0.fieldQ * 1240781149) {
                if (var3 != 3) {
                    return;
                }

                ItemPile.insertAction(var0.fieldDk, "", 29, 0, 0, var0.id * -1866039137, (byte) -60);
            }

            if (var0.fieldQ * 1240781149 == 6) {
                if (var3 != 3) {
                    throw new IllegalStateException();
                }

                if (Client.fieldJy == null) {
                    if (var3 != 3) {
                        throw new IllegalStateException();
                    }

                    ItemPile.insertAction(var0.fieldDk, "", 30, 0, -1, var0.id * -1866039137, (byte) -92);
                }
            }

            int var5;
            int var6;
            int var8;
            boolean var10000;
            int var20;
            if (var0.type * -729517859 == 2) {
                if (var3 != 3) {
                    throw new IllegalStateException();
                }

                var20 = 0;

                for (var5 = 0; var5 < var0.height * -1004717071; ++var5) {
                    for (var6 = 0; var6 < var0.width * 805002637; ++var6) {
                        int var7 = (32 + var0.fieldBk * -2111277319) * var6;
                        var8 = var5 * (32 + var0.fieldBf * -842585833);
                        if (var20 < 20) {
                            if (var3 != 3) {
                                throw new IllegalStateException();
                            }

                            var7 += var0.xSprites[var20];
                            var8 += var0.ySprites[var20];
                        }

                        if (var1 >= var7) {
                            if (var3 != 3) {
                                return;
                            }

                            if (var2 >= var8) {
                                if (var3 != 3) {
                                    throw new IllegalStateException();
                                }

                                if (var1 < var7 + 32) {
                                    if (var3 != 3) {
                                        throw new IllegalStateException();
                                    }

                                    if (var2 < 32 + var8) {
                                        if (var3 != 3) {
                                            throw new IllegalStateException();
                                        }

                                        Client.fieldGg = -400161139 * var20;
                                        classBy.fieldGu = var0;
                                        if (var0.fieldDr[var20] > 0) {
                                            label559:
                                            {
                                                if (var3 != 3) {
                                                    return;
                                                }

                                                ItemDefinition var9 = AnimationSequence.getItemDefinition(var0.fieldDr[var20] - 1, -756689105);
                                                if (1 == Client.itemSelectionStatus * 279939385) {
                                                    if (var3 != 3) {
                                                        throw new IllegalStateException();
                                                    }

                                                    if (UtilClass6.method64(UtilClass38.getWidgetConfig(var0, 2070855251), 1078584907)) {
                                                        if (var3 != 3) {
                                                            throw new IllegalStateException();
                                                        }

                                                        if (var0.id * -1866039137 == UtilClass6.selectedItemWidgetId * 1795747025) {
                                                            if (var3 != 3) {
                                                                return;
                                                            }

                                                            if (var20 == UtilClass41.selectedItemIndex * 1003817045) {
                                                                break label559;
                                                            }
                                                        }

                                                        ItemPile.insertAction(StringConstants.fieldEk, Client.fieldIt + " " + UtilClass41.fieldM + " " + Buffer.method67(16748608, -1499648027) + var9.fieldX, 31, var9.id * -1571020975, var20, var0.id * -1866039137, (byte) -93);
                                                        break label559;
                                                    }
                                                }

                                                if (Client.isSpellSelected) {
                                                    if (var3 != 3) {
                                                        return;
                                                    }

                                                    int var11 = UtilClass38.getWidgetConfig(var0, 2070855251);
                                                    if ((var11 >> 30 & 1) != 0) {
                                                        if (var3 != 3) {
                                                            throw new IllegalStateException();
                                                        }

                                                        var10000 = true;
                                                    } else {
                                                        var10000 = false;
                                                    }

                                                    boolean var10 = var10000;
                                                    if (var10) {
                                                        if (16 == (UtilClass40.selectedSpellTargets * 1512402281 & 16)) {
                                                            if (var3 != 3) {
                                                                throw new IllegalStateException();
                                                            }

                                                            ItemPile.insertAction(Client.spellAction, Client.fieldJc + " " + UtilClass41.fieldM + " " + Buffer.method67(16748608, -983068456) + var9.fieldX, 32, var9.id * -1571020975, var20, var0.id * -1866039137, (byte) -28);
                                                        }
                                                        break label559;
                                                    }
                                                }

                                                String[] var25 = var9.fieldAh;
                                                if (Client.fieldJo) {
                                                    if (var3 != 3) {
                                                        throw new IllegalStateException();
                                                    }

                                                    var25 = UtilClass24.method3(var25, -1426559463);
                                                }

                                                int var12 = UtilClass38.getWidgetConfig(var0, 2070855251);
                                                if (0 != (var12 >> 30 & 1)) {
                                                    if (var3 != 3) {
                                                        throw new IllegalStateException();
                                                    }

                                                    var10000 = true;
                                                } else {
                                                    var10000 = false;
                                                }

                                                boolean var26 = var10000;
                                                if (var26) {
                                                    if (var3 != 3) {
                                                        throw new IllegalStateException();
                                                    }

                                                    for (int var13 = 4; var13 >= 3; --var13) {
                                                        if (null != var25) {
                                                            if (var3 != 3) {
                                                                throw new IllegalStateException();
                                                            }

                                                            if (var25[var13] != null) {
                                                                if (var3 != 3) {
                                                                    throw new IllegalStateException();
                                                                }

                                                                byte var14;
                                                                if (3 == var13) {
                                                                    var14 = 36;
                                                                } else {
                                                                    var14 = 37;
                                                                }

                                                                ItemPile.insertAction(var25[var13], Buffer.method67(16748608, 1341875985) + var9.fieldX, var14, var9.id * -1571020975, var20, var0.id * -1866039137, (byte) -65);
                                                                continue;
                                                            }
                                                        }

                                                        if (4 == var13) {
                                                            ItemPile.insertAction(StringConstants.fieldI, Buffer.method67(16748608, 505967043) + var9.fieldX, 37, var9.id * -1571020975, var20, var0.id * -1866039137, (byte) -37);
                                                        }
                                                    }
                                                }

                                                int var28 = UtilClass38.getWidgetConfig(var0, 2070855251);
                                                if (0 != (var28 >> 31 & 1)) {
                                                    if (var3 != 3) {
                                                        return;
                                                    }

                                                    var10000 = true;
                                                } else {
                                                    var10000 = false;
                                                }

                                                boolean var27 = var10000;
                                                if (var27) {
                                                    if (var3 != 3) {
                                                        return;
                                                    }

                                                    ItemPile.insertAction(StringConstants.fieldEk, Buffer.method67(16748608, -211300135) + var9.fieldX, 38, var9.id * -1571020975, var20, var0.id * -1866039137, (byte) -97);
                                                }

                                                int var16 = UtilClass38.getWidgetConfig(var0, 2070855251);
                                                if ((var16 >> 30 & 1) != 0) {
                                                    if (var3 != 3) {
                                                        throw new IllegalStateException();
                                                    }

                                                    var10000 = true;
                                                } else {
                                                    var10000 = false;
                                                }

                                                boolean var15 = var10000;
                                                int var17;
                                                byte var18;
                                                if (var15) {
                                                    if (var3 != 3) {
                                                        return;
                                                    }

                                                    if (null != var25) {
                                                        for (var17 = 2; var17 >= 0; --var17) {
                                                            if (var3 != 3) {
                                                                throw new IllegalStateException();
                                                            }

                                                            if (var25[var17] != null) {
                                                                if (var3 != 3) {
                                                                    return;
                                                                }

                                                                var18 = 0;
                                                                if (var17 == 0) {
                                                                    var18 = 33;
                                                                }

                                                                if (1 == var17) {
                                                                    if (var3 != 3) {
                                                                        return;
                                                                    }

                                                                    var18 = 34;
                                                                }

                                                                if (2 == var17) {
                                                                    var18 = 35;
                                                                }

                                                                ItemPile.insertAction(var25[var17], Buffer.method67(16748608, -1984397223) + var9.fieldX, var18, var9.id * -1571020975, var20, var0.id * -1866039137, (byte) -85);
                                                            }
                                                        }
                                                    }
                                                }

                                                var25 = var0.fieldBn;
                                                if (Client.fieldJo) {
                                                    if (var3 != 3) {
                                                        throw new IllegalStateException();
                                                    }

                                                    var25 = UtilClass24.method3(var25, -1659841739);
                                                }

                                                if (var25 != null) {
                                                    for (var17 = 4; var17 >= 0; --var17) {
                                                        if (var3 != 3) {
                                                            throw new IllegalStateException();
                                                        }

                                                        if (var25[var17] != null) {
                                                            var18 = 0;
                                                            if (0 == var17) {
                                                                if (var3 != 3) {
                                                                    throw new IllegalStateException();
                                                                }

                                                                var18 = 39;
                                                            }

                                                            if (1 == var17) {
                                                                if (var3 != 3) {
                                                                    return;
                                                                }

                                                                var18 = 40;
                                                            }

                                                            if (2 == var17) {
                                                                if (var3 != 3) {
                                                                    throw new IllegalStateException();
                                                                }

                                                                var18 = 41;
                                                            }

                                                            if (var17 == 3) {
                                                                if (var3 != 3) {
                                                                    throw new IllegalStateException();
                                                                }

                                                                var18 = 42;
                                                            }

                                                            if (var17 == 4) {
                                                                if (var3 != 3) {
                                                                    throw new IllegalStateException();
                                                                }

                                                                var18 = 43;
                                                            }

                                                            ItemPile.insertAction(var25[var17], Buffer.method67(16748608, -78645642) + var9.fieldX, var18, var9.id * -1571020975, var20, var0.id * -1866039137, (byte) -97);
                                                        }
                                                    }
                                                }

                                                ItemPile.insertAction(StringConstants.EXAMINE, Buffer.method67(16748608, -1473814822) + var9.fieldX, 1005, var9.id * -1571020975, var20, var0.id * -1866039137, (byte) -126);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        ++var20;
                    }
                }
            }

            if (var0.fieldK) {
                if (var3 != 3) {
                    throw new IllegalStateException();
                }

                if (Client.isSpellSelected) {
                    if (var3 != 3) {
                        return;
                    }

                    if (classX.method198(UtilClass38.getWidgetConfig(var0, 2070855251), 2095271657)) {
                        if (var3 != 3) {
                            throw new IllegalStateException();
                        }

                        if (32 == (UtilClass40.selectedSpellTargets * 1512402281 & 32)) {
                            if (var3 != 3) {
                                throw new IllegalStateException();
                            }

                            ItemPile.insertAction(Client.spellAction, Client.fieldJc + " " + UtilClass41.fieldM + " " + var0.fieldBa, 58, 0, var0.index * 1176560255, var0.id * -1866039137, (byte) -4);
                        }
                    }
                } else {
                    for (var20 = 9; var20 >= 5; --var20) {
                        String var21 = UtilClass10.method77(var0, var20, (byte) 1);
                        if (var21 != null) {
                            if (var3 != 3) {
                                return;
                            }

                            ItemPile.insertAction(var21, var0.fieldBa, 1007, var20 + 1, var0.index * 1176560255, var0.id * -1866039137, (byte) -60);
                        }
                    }

                    var4 = classCs.method304(var0, 469151008);
                    if (null != var4) {
                        if (var3 != 3) {
                            throw new IllegalStateException();
                        }

                        ItemPile.insertAction(var4, var0.fieldBa, 25, 0, var0.index * 1176560255, var0.id * -1866039137, (byte) -40);
                    }

                    for (var5 = 4; var5 >= 0; --var5) {
                        if (var3 != 3) {
                            throw new IllegalStateException();
                        }

                        var8 = UtilClass38.getWidgetConfig(var0, 2070855251);
                        if (0 != (var8 >> 1 + var5 & 1)) {
                            if (var3 != 3) {
                                throw new IllegalStateException();
                            }

                            var10000 = true;
                        } else {
                            var10000 = false;
                        }

                        String var23;
                        label572:
                        {
                            boolean var24 = var10000;
                            if (!var24) {
                                if (var3 != 3) {
                                    throw new IllegalStateException();
                                }

                                if (var0.fieldCz == null) {
                                    if (var3 != 3) {
                                        throw new IllegalStateException();
                                    }

                                    var23 = null;
                                    break label572;
                                }
                            }

                            if (var0.fieldBv != null) {
                                if (var3 != 3) {
                                    throw new IllegalStateException();
                                }

                                if (var0.fieldBv.length > var5) {
                                    if (var3 != 3) {
                                        return;
                                    }

                                    if (var0.fieldBv[var5] != null) {
                                        if (var3 != 3) {
                                            throw new IllegalStateException();
                                        }

                                        if (var0.fieldBv[var5].trim().length() != 0) {
                                            var23 = var0.fieldBv[var5];
                                            break label572;
                                        }

                                        if (var3 != 3) {
                                            throw new IllegalStateException();
                                        }
                                    }
                                }
                            }

                            var23 = null;
                        }

                        if (null != var23) {
                            if (var3 != 3) {
                                throw new IllegalStateException();
                            }

                            ItemPile.insertAction(var23, var0.fieldBa, 57, var5 + 1, var0.index * 1176560255, var0.id * -1866039137, (byte) -39);
                        }
                    }

                    var6 = UtilClass38.getWidgetConfig(var0, 2070855251);
                    if ((var6 & 1) != 0) {
                        if (var3 != 3) {
                            throw new IllegalStateException();
                        }

                        var10000 = true;
                    } else {
                        var10000 = false;
                    }

                    boolean var22 = var10000;
                    if (var22) {
                        if (var3 != 3) {
                            throw new IllegalStateException();
                        }

                        ItemPile.insertAction(StringConstants.fieldW, "", 30, 0, var0.index * 1176560255, var0.id * -1866039137, (byte) -126);
                    }
                }
            }

        } catch (RuntimeException var19) {
            throw ClanMate.error(var19, "o.ck(" + ')');
        }
    }

    public static void method121(ReferenceTable var0, ReferenceTable var1, ReferenceTable var2, ReferenceTable var3, int var4) {
        try {
            Widget.widgetRefTable = var0;
            classEr.fieldB = var1;
            classDc.fieldL = var2;
            UtilClass32.fieldM = var3;
            Widget.widgets = new Widget[Widget.widgetRefTable.getFileCount(2032617000)][];
            Widget.fieldV = new boolean[Widget.widgetRefTable.getFileCount(2060301196)];
        } catch (RuntimeException var5) {
            throw ClanMate.error(var5, "o.a(" + ')');
        }
    }

    static final void method122(Character var0, int var1, int var2) {
        try {
            classEs.method190(var0.strictX * 404428603, var0.strictY * -1490664571, var1, 818412146);
        } catch (RuntimeException var3) {
            throw ClanMate.error(var3, "o.ao(" + ')');
        }
    }

    static final void method123(int var0, int var1) {
        try {
            if (var0 < 0) {
                if (var1 <= 1293330857) {
                    throw new IllegalStateException();
                }
            } else {
                int var2 = Client.menuArg1[var0];
                int var3 = Client.menuArg2[var0];
                int var4 = Client.menuOpcodes[var0];
                int var5 = Client.menuArg0[var0];
                String var6 = Client.menuOptions[var0];
                String var7 = Client.menuNouns[var0];
                WidgetNode.processAction( var2,
                                          var3,
                                          var4,
                                          var5,
                                          var6,
                                          var7,
                                          classEt.fieldX * -1590834513,
                                          classEt.fieldO * 1708223245,
                                          ( byte ) -124 );
            }
        } catch (RuntimeException var8) {
            throw ClanMate.error(var8, "o.bx(" + ')');
        }
    }
}
