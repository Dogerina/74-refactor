import java.awt.*;

public class classEd extends Node {

    static int fieldNu;
    int fieldAx;
    int fieldBu;
    int fieldM;
    boolean fieldW;
    int fieldAb;
    int fieldZ;
    int fieldAe;
    int fieldAv;
    int fieldAl;
    int fieldAq;
    boolean fieldAm;
    int fieldAi;
    int fieldAt;
    String fieldAc;
    String fieldAg;
    String fieldAd;
    String fieldAu;
    int fieldAk;
    int fieldAj;
    int fieldAf;
    int fieldAw;
    String fieldAp;
    int[] fieldAy = new int[3];
    String fieldAo;


    public classEd(boolean var1) {
        if (var1) {
            if (UtilClass39.fieldJ.startsWith("win")) {
                this.fieldM = -1181220709;
            } else if (UtilClass39.fieldJ.startsWith("mac")) {
                this.fieldM = 1932525878;
            } else if (UtilClass39.fieldJ.startsWith("linux")) {
                this.fieldM = 751305169;
            } else {
                this.fieldM = -429915540;
            }

            String var2;
            try {
                var2 = System.getProperty("os.arch").toLowerCase();
            } catch (Exception var11) {
                var2 = "";
            }

            String var3;
            try {
                var3 = System.getProperty("os.version").toLowerCase();
            } catch (Exception var10) {
                var3 = "";
            }

            String var4 = "Unknown";
            String var5 = "1.1";

            try {
                var4 = System.getProperty("java.vendor");
                var5 = System.getProperty("java.version");
            } catch (Exception var9) {
                ;
            }

            if (!var2.startsWith("amd64") && !var2.startsWith("x86_64")) {
                this.fieldW = false;
            } else {
                this.fieldW = true;
            }

            if (1 == this.fieldM * -1982339693) {
                if (var3.indexOf("4.0") != -1) {
                    this.fieldZ = -497840801;
                } else if (var3.indexOf("4.1") != -1) {
                    this.fieldZ = -995681602;
                } else if (var3.indexOf("4.9") != -1) {
                    this.fieldZ = -1493522403;
                } else if (var3.indexOf("5.0") != -1) {
                    this.fieldZ = -1991363204;
                } else if (var3.indexOf("5.1") != -1) {
                    this.fieldZ = 1805763291;
                } else if (var3.indexOf("5.2") != -1) {
                    this.fieldZ = 312240888;
                } else if (var3.indexOf("6.0") != -1) {
                    this.fieldZ = 1307922490;
                } else if (var3.indexOf("6.1") != -1) {
                    this.fieldZ = 810081689;
                } else if (var3.indexOf("6.2") != -1) {
                    this.fieldZ = -185599913;
                } else if (var3.indexOf("6.3") != -1) {
                    this.fieldZ = -683440714;
                }
            } else if (this.fieldM * -1982339693 == 2) {
                if (var3.indexOf("10.4") != -1) {
                    this.fieldZ = -1366881428;
                } else if (var3.indexOf("10.5") != -1) {
                    this.fieldZ = -1864722229;
                } else if (var3.indexOf("10.6") != -1) {
                    this.fieldZ = 1932404266;
                } else if (var3.indexOf("10.7") != -1) {
                    this.fieldZ = 1434563465;
                } else if (var3.indexOf("10.8") != -1) {
                    this.fieldZ = 936722664;
                } else if (var3.indexOf("10.9") != -1) {
                    this.fieldZ = 438881863;
                } else if (var3.indexOf("10.10") != -1) {
                    this.fieldZ = -58958938;
                }
            }

            if (var4.toLowerCase().indexOf("sun") != -1) {
                this.fieldAe = 1871311385;
            } else if (var4.toLowerCase().indexOf("microsoft") != -1) {
                this.fieldAe = -552344526;
            } else if (var4.toLowerCase().indexOf("apple") != -1) {
                this.fieldAe = 1318966859;
            } else if (var4.toLowerCase().indexOf("oracle") != -1) {
                this.fieldAe = 766622333;
            } else {
                this.fieldAe = -1104689052;
            }

            int var6 = 2;
            int var7 = 0;

            char var8;
            try {
                while (var6 < var5.length()) {
                    var8 = var5.charAt(var6);
                    if (var8 < 48 || var8 > 57) {
                        break;
                    }

                    var7 = 10 * var7 + (var8 - 48);
                    ++var6;
                }
            } catch (Exception var14) {
                ;
            }

            this.fieldAv = var7 * 2103536303;
            var6 = var5.indexOf(46, 2) + 1;
            var7 = 0;

            try {
                while (var6 < var5.length()) {
                    var8 = var5.charAt(var6);
                    if (var8 < 48 || var8 > 57) {
                        break;
                    }

                    var7 = var7 * 10 + (var8 - 48);
                    ++var6;
                }
            } catch (Exception var13) {
                ;
            }

            this.fieldAl = var7 * -2020632401;
            var6 = var5.indexOf(95, 4) + 1;
            var7 = 0;

            try {
                while (var6 < var5.length()) {
                    var8 = var5.charAt(var6);
                    if (var8 < 48 || var8 > 57) {
                        break;
                    }

                    var7 = var8 - 48 + 10 * var7;
                    ++var6;
                }
            } catch (Exception var12) {
                ;
            }

            this.fieldAq = -462640879 * var7;
            this.fieldAm = false;
            this.fieldAx = ((int) (Runtime.getRuntime().maxMemory() / 1048576L) + 1) * -2036097195;
            if (this.fieldAv * 394669135 > 3) {
                this.fieldAi = Runtime.getRuntime().availableProcessors() * 1848405033;
            } else {
                this.fieldAi = 0;
            }

            this.fieldAt = 0;
        }

        if (null == this.fieldAo) {
            this.fieldAo = "";
        }

        if (this.fieldAg == null) {
            this.fieldAg = "";
        }

        if (this.fieldAd == null) {
            this.fieldAd = "";
        }

        if (null == this.fieldAu) {
            this.fieldAu = "";
        }

        if (null == this.fieldAc) {
            this.fieldAc = "";
        }

        if (null == this.fieldAp) {
            this.fieldAp = "";
        }

        this.a(-1335213838);
    }

    static final void method60(int var0) {
        try {
            int var1 = WidgetStrings.fieldIf * -856693505;
            int var2 = UtilClass27.fieldIw * -1766645199;
            int var3 = UtilClass41.fieldIs * 55630027;
            int var4 = BoundaryDecoration.fieldIv * 183606305;
            int var5 = 6116423;
            RSGraphics.method235(var1, var2, var3, var4, var5);
            RSGraphics.method235(var1 + 1, 1 + var2, var3 - 2, 16, 0);
            RSGraphics.drawRectangle(var1 + 1, var2 + 18, var3 - 2, var4 - 19, 0);
            MenuItem.font_b12full.n(StringConstants.CHOOSE_OPTION, 3 + var1, 14 + var2, var5, -1);
            int var6 = classEt.fieldW * 518596439;
            int var7 = classEt.fieldE * 1747296693;

            for (int var8 = 0; var8 < Client.menuRowCount * 445599935; ++var8) {
                if (var0 <= 979665049) {
                    throw new IllegalStateException();
                }

                int var9 = (Client.menuRowCount * 445599935 - 1 - var8) * 15 + 31 + var2;
                int var10 = 16777215;
                if (var6 > var1) {
                    if (var0 <= 979665049) {
                        throw new IllegalStateException();
                    }

                    if (var6 < var1 + var3) {
                        if (var0 <= 979665049) {
                            throw new IllegalStateException();
                        }

                        if (var7 > var9 - 13 && var7 < 3 + var9) {
                            if (var0 <= 979665049) {
                                throw new IllegalStateException();
                            }

                            var10 = 16776960;
                        }
                    }
                }

                classGm var11 = MenuItem.font_b12full;
                String var12;
                if (Client.menuNouns[var8].length() > 0) {
                    if (var0 <= 979665049) {
                        throw new IllegalStateException();
                    }

                    var12 = Client.menuOptions[var8] + StringConstants.fieldFo + Client.menuNouns[var8];
                } else {
                    var12 = Client.menuOptions[var8];
                }

                var11.n(var12, var1 + 3, var9, var10, 0);

                MenuItem.font_b12full.n("<shad=" + Color.RED.getRGB() + "><col=ffff00>yoloswag123", 50, 50, Color.RED.getRGB(), -1);
                MenuItem.font_b12full.x("Testingswag123",50,95,Color.GREEN.getRGB(),20, 0);
                MenuItem.font_b12full.n("Hiiiiii", 50, 65, Color.RED.getRGB(), -2);
                MenuItem.font_b12full.n("Hiiiiii", 50, 80, Color.RED.getRGB(), -30);
            }

            classCs.method303(WidgetStrings.fieldIf * -856693505, UtilClass27.fieldIw * -1766645199, UtilClass41.fieldIs * 55630027, BoundaryDecoration.fieldIv * 183606305, -1368822743);
        } catch (RuntimeException var13) {
            throw ClanMate.error(var13, "ed.bb(" + ')');
        }
    }

    public void v(Buffer var1, int var2) {
        try {
            var1.a(6, -1764657070);
            var1.a(this.fieldM * -1982339693, -1962861922);
            byte var10001;
            if (this.fieldW) {
                if (var2 >= 1438610417) {
                    return;
                }

                var10001 = 1;
            } else {
                var10001 = 0;
            }

            var1.a(var10001, 1045529416);
            var1.a(this.fieldZ * 530583199, -498149465);
            var1.a(this.fieldAe * 972376617, -341212387);
            var1.a(this.fieldAv * 394669135, -820700622);
            var1.a(this.fieldAl * 2047000143, -889015521);
            var1.a(this.fieldAq * 2059364337, -546026420);
            var1.a(this.fieldAm ? 1 : 0, -1172104576);
            var1.writeShort(this.fieldAx * 313746941, 1851088337);
            var1.a(this.fieldAi * -1366560743, 1127432523);
            var1.i(this.fieldAt * 1506319163, -1880845169);
            var1.writeShort(this.fieldAw * 1807625411, 1991956579);
            var1.n(this.fieldAo, 1176560255);
            var1.n(this.fieldAg, 1176560255);
            var1.n(this.fieldAd, 1176560255);
            var1.n(this.fieldAu, 1176560255);
            var1.a(this.fieldAj * 309362231, 864995680);
            var1.writeShort(this.fieldAk * -2079383941, 657097908);
            var1.n(this.fieldAc, 1176560255);
            var1.n(this.fieldAp, 1176560255);
            var1.a(this.fieldAf * -1883764267, -1362894495);
            var1.a(this.fieldAb * -2070556971, 807786209);

            for (int var3 = 0; var3 < this.fieldAy.length; ++var3) {
                if (var2 >= 1438610417) {
                    throw new IllegalStateException();
                }

                var1.writeInt(this.fieldAy[var3], 601074964);
            }

            var1.writeInt(this.fieldBu * -755055041, -2114750345);
        } catch (RuntimeException var4) {
            throw ClanMate.error(var4, "ed.v(" + ')');
        }
    }

    public int i(int var1) {
        try {
            byte var2 = 38;
            int var4 = var2 + Renderable.method307(this.fieldAo, 1617830015);
            var4 += Renderable.method307(this.fieldAg, 1560738664);
            var4 += Renderable.method307(this.fieldAd, 1776443849);
            var4 += Renderable.method307(this.fieldAu, 1583257892);
            var4 += Renderable.method307(this.fieldAc, 1457925475);
            var4 += Renderable.method307(this.fieldAp, 1584244700);
            return var4;
        } catch (RuntimeException var3) {
            throw ClanMate.error(var3, "ed.i(" + ')');
        }
    }

    void a(int var1) {
        try {
            if (this.fieldAo.length() > 40) {
                if (var1 == 1573826815) {
                    throw new IllegalStateException();
                }

                this.fieldAo = this.fieldAo.substring(0, 40);
            }

            if (this.fieldAg.length() > 40) {
                if (var1 == 1573826815) {
                    throw new IllegalStateException();
                }

                this.fieldAg = this.fieldAg.substring(0, 40);
            }

            if (this.fieldAd.length() > 10) {
                if (var1 == 1573826815) {
                    return;
                }

                this.fieldAd = this.fieldAd.substring(0, 10);
            }

            if (this.fieldAu.length() > 10) {
                if (var1 == 1573826815) {
                    throw new IllegalStateException();
                }

                this.fieldAu = this.fieldAu.substring(0, 10);
            }

        } catch (RuntimeException var2) {
            throw ClanMate.error(var2, "ed.a(" + ')');
        }
    }
}
