public class classBp {

    static float fieldE;
    static int[][] fieldW = new int[2][8];
    static int fieldN;
    static float[][] fieldM = new float[2][8];
    int[][][] fieldI = new int[2][2][4];
    int[][][] fieldB = new int[2][2][4];
    int[] fieldL = new int[2];
    int[] fieldA = new int[2];

    static float method262(float var0) {
        float var1 = 32.703197F * (float) Math.pow(2.0D, (double) var0);
        return var1 * 3.1415927F / 11025.0F;
    }

    float a(int var1, int var2, float var3) {
        float var4 = (float) this.fieldB[var1][0][var2] + var3 * (float) (this.fieldB[var1][1][var2] - this.fieldB[var1][0][var2]);
        var4 *= 0.0015258789F;
        return 1.0F - (float) Math.pow(10.0D, (double) (-var4 / 20.0F));
    }

    float i(int var1, int var2, float var3) {
        float var4 = (float) this.fieldI[var1][0][var2] + var3 * (float) (this.fieldI[var1][1][var2] - this.fieldI[var1][0][var2]);
        var4 *= 1.2207031E-4F;
        return method262(var4);
    }

    int b(int var1, float var2) {
        float var3;
        if (var1 == 0) {
            var3 = (float) this.fieldL[0] + (float) (this.fieldL[1] - this.fieldL[0]) * var2;
            var3 *= 0.0030517578F;
            fieldE = (float) Math.pow(0.1D, (double) (var3 / 20.0F));
            fieldN = (int) (fieldE * 65536.0F);
        }

        if (this.fieldA[var1] == 0) {
            return 0;
        } else {
            var3 = this.a(var1, 0, var2);
            fieldM[var1][0] = -2.0F * var3 * (float) Math.cos((double) this.i(var1, 0, var2));
            fieldM[var1][1] = var3 * var3;

            int var4;
            for (var4 = 1; var4 < this.fieldA[var1]; ++var4) {
                var3 = this.a(var1, var4, var2);
                float var5 = -2.0F * var3 * (float) Math.cos((double) this.i(var1, var4, var2));
                float var6 = var3 * var3;
                fieldM[var1][var4 * 2 + 1] = fieldM[var1][var4 * 2 - 1] * var6;
                fieldM[var1][var4 * 2] = fieldM[var1][var4 * 2 - 1] * var5 + fieldM[var1][var4 * 2 - 2] * var6;

                for (int var7 = var4 * 2 - 1; var7 >= 2; --var7) {
                    fieldM[var1][var7] += fieldM[var1][var7 - 1] * var5 + fieldM[var1][var7 - 2] * var6;
                }

                fieldM[var1][1] += fieldM[var1][0] * var5 + var6;
                fieldM[var1][0] += var5;
            }

            if (var1 == 0) {
                for (var4 = 0; var4 < this.fieldA[0] * 2; ++var4) {
                    fieldM[0][var4] *= fieldE;
                }
            }

            for (var4 = 0; var4 < this.fieldA[var1] * 2; ++var4) {
                fieldW[var1][var4] = (int) (fieldM[var1][var4] * 65536.0F);
            }

            return this.fieldA[var1] * 2;
        }
    }

    final void l(Buffer var1, classAy var2) {
        int var3 = var1.readUByte(-257782344);
        this.fieldA[0] = var3 >> 4;
        this.fieldA[1] = var3 & 15;
        if (var3 != 0) {
            this.fieldL[0] = var1.readUShort(829603351);
            this.fieldL[1] = var1.readUShort(829603351);
            int var4 = var1.readUByte(666318657);

            int var5;
            int var6;
            for (var5 = 0; var5 < 2; ++var5) {
                for (var6 = 0; var6 < this.fieldA[var5]; ++var6) {
                    this.fieldI[var5][0][var6] = var1.readUShort(829603351);
                    this.fieldB[var5][0][var6] = var1.readUShort(829603351);
                }
            }

            for (var5 = 0; var5 < 2; ++var5) {
                for (var6 = 0; var6 < this.fieldA[var5]; ++var6) {
                    if ((var4 & 1 << var5 * 4 << var6) != 0) {
                        this.fieldI[var5][1][var6] = var1.readUShort(829603351);
                        this.fieldB[var5][1][var6] = var1.readUShort(829603351);
                    } else {
                        this.fieldI[var5][1][var6] = this.fieldI[var5][0][var6];
                        this.fieldB[var5][1][var6] = this.fieldB[var5][0][var6];
                    }
                }
            }

            if (var4 != 0 || this.fieldL[1] != this.fieldL[0]) {
                var2.v(var1);
            }
        } else {
            int[] var7 = this.fieldL;
            this.fieldL[1] = 0;
            var7[0] = 0;
        }

    }
}
