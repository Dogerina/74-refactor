import java.util.Iterator;

public class classGp implements Iterable {

    Node fieldA = new Node();

    public classGp() {
        this.fieldA.next = this.fieldA;
        this.fieldA.prev = this.fieldA;
    }

    public Node v() {
        return this.i(null);
    }

    Node i(Node var1) {
        Node var2 = var1 == null ? this.fieldA.next : var1;
        return var2 == this.fieldA ? null : var2;
    }

    public void a(Node var1) {
        if (var1.prev != null) {
            var1.delete();
        }

        var1.prev = this.fieldA.prev;
        var1.next = this.fieldA;
        var1.prev.next = var1;
        var1.next.prev = var1;
    }

    @Override
    public Iterator iterator() {
        return new classGf(this);
    }
}
