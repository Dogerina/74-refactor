import java.awt.*;
import java.io.*;

public class DynamicObject extends Renderable {

    static CompressedImage[] worldSelectorArrows;
    static byte[][] fieldDk;
    static int fieldBd;
    int fieldL;
    int fieldV;
    int fieldI;
    int fieldN;
    AnimationSequence fieldW;
    int fieldE;
    int fieldM;
    int fieldA;
    int fieldB;


    DynamicObject(int var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8, Renderable var9) {
        this.fieldA = -1862197825 * var1;
        this.fieldV = 1951715283 * var2;
        this.fieldI = var3 * -1436157935;
        this.fieldB = 2076509429 * var4;
        this.fieldL = 1637647001 * var5;
        this.fieldM = 224589007 * var6;
        if (var7 != -1) {
            this.fieldW = Projectile.getAnimationSequence(var7, -1665806075);
            this.fieldE = 0;
            this.fieldN = Client.engineCycle * -911169251 - -31302455;
            if (888956053 * this.fieldW.fieldU == 0 && var9 != null && var9 instanceof DynamicObject) {
                DynamicObject var10 = (DynamicObject) var9;
                if (this.fieldW == var10.fieldW) {
                    this.fieldE = 1 * var10.fieldE;
                    this.fieldN = var10.fieldN * 1;
                    return;
                }
            }

            if (var8 && -1307272215 * this.fieldW.fieldS != -1) {
                this.fieldE = (int) (Math.random() * (double) this.fieldW.fieldM.length) * 1657668353;
                this.fieldN -= (int) (Math.random() * (double) this.fieldW.fieldE[this.fieldE * 1446706433]) * -31302455;
            }
        }

    }

    static final void method136(int var0, Player var1, int var2, int var3) {
        try {
            int var4;
            if (0 != (var2 & 16)) {
                if (var3 != -1571534834) {
                    return;
                }

                var4 = Client.packet.readInvertedUByte((byte) 0);
                byte[] var5 = new byte[var4];
                Buffer var6 = new Buffer(var5);
                Client.packet.read(var5, 0, var4, 1997945954);
                Client.fieldHk[var0] = var6;
                var1.read(var6, -1225537876);
            }

            if ((var2 & 128) != 0) {
                var1.targetIndex = Client.packet.ac((byte) 56) * 2015474651;
                if (var1.targetIndex * -1206972333 == '\uffff') {
                    if (var3 != -1571534834) {
                        throw new IllegalStateException();
                    }

                    var1.targetIndex = -2015474651;
                }
            }

            if (0 != (var2 & 1)) {
                if (var3 != -1571534834) {
                    return;
                }

                var1.fieldAf = Client.packet.ap(1068581517) * 1722699207;
                var1.fieldAb = Client.packet.readUShort(829603351) * -1312598379;
            }

            int var16;
            if (0 != (var2 & 4)) {
                if (var3 != -1571534834) {
                    return;
                }

                var4 = Client.packet.readUShort(829603351);
                var16 = Client.packet.ao((byte) 75);
                var1.registerHit(var4, var16, Client.engineCycle * -1223223371, 259182187);
                var1.healthBarCycle = 1201629052 + Client.engineCycle * 1859706401;
                var1.health = Client.packet.readUByte(-1647069651) * -1198485193;
                var1.maxHealth = Client.packet.readInvertedUByte((byte) 0) * 309829455;
            }

            if (0 != (var2 & 512)) {
                if (var3 != -1571534834) {
                    throw new IllegalStateException();
                }

                var4 = Client.packet.ap(-1894050309);
                var16 = Client.packet.aw((byte) 7);
                var1.registerHit(var4, var16, Client.engineCycle * -1223223371, -568209532);
                var1.healthBarCycle = 1201629052 + Client.engineCycle * 1859706401;
                var1.health = Client.packet.ao((byte) 86) * -1198485193;
                var1.maxHealth = Client.packet.aw((byte) 7) * 309829455;
            }

            if (0 != (var2 & 64)) {
                if (var3 != -1571534834) {
                    return;
                }

                var1.overheadText = Client.packet.readString(432711714);
                if (var1.overheadText.charAt(0) == 126) {
                    if (var3 != -1571534834) {
                        return;
                    }

                    var1.overheadText = var1.overheadText.substring(1);
                    JagSocket.addMessage(2, var1.name, var1.overheadText, (byte) 16);
                } else if (var1 == Renderable.localPlayer) {
                    if (var3 != -1571534834) {
                        throw new IllegalStateException();
                    }

                    JagSocket.addMessage(2, var1.name, var1.overheadText, (byte) 16);
                }

                var1.fieldAm = false;
                var1.fieldAi = 0;
                var1.fieldAa = 0;
                var1.fieldAz = -547011178;
            }

            if (0 != (var2 & 1024)) {
                if (var3 != -1571534834) {
                    return;
                }

                var1.fieldBe = Client.packet.ao((byte) 108) * -1665532031;
                var1.fieldBc = Client.packet.ao((byte) 34) * 692803995;
                var1.fieldBb = Client.packet.aw((byte) 7) * 19885597;
                var1.fieldBk = Client.packet.ao((byte) 105) * -1120853325;
                var1.fieldBf = (Client.packet.readUShort(829603351) + Client.engineCycle * -1223223371) * 1271855895;
                var1.fieldBg = (Client.packet.readUShort(829603351) + Client.engineCycle * -1223223371) * -1734719039;
                var1.fieldBt = Client.packet.readUByte(-351361456) * 1262183549;
                var1.queueSize = -927674979;
                var1.fieldCt = 0;
            }

            if (0 != (var2 & 256)) {
                if (var3 != -1571534834) {
                    return;
                }

                var1.fieldBp = Client.packet.ab(1437321420) * 1712363827;
                var4 = Client.packet.bl(-1370254784);
                var1.fieldBr = -1148182407 * (var4 >> 16);
                var1.fieldBz = 794364867 * ((var4 & '\uffff') + Client.engineCycle * -1223223371);
                var1.fieldBw = 0;
                var1.fieldBm = 0;
                if (var1.fieldBz * 1643494635 > Client.engineCycle * -1223223371) {
                    if (var3 != -1571534834) {
                        throw new IllegalStateException();
                    }

                    var1.fieldBw = -966935865;
                }

                if ('\uffff' == var1.fieldBp * -1404237317) {
                    if (var3 != -1571534834) {
                        return;
                    }

                    var1.fieldBp = -1712363827;
                }
            }

            if (0 != (var2 & 8)) {
                if (var3 != -1571534834) {
                    throw new IllegalStateException();
                }

                var4 = Client.packet.ac((byte) 84);
                UnknownEnum2 var17 = classN.ValueOf(UnknownEnum3.getEnum2Values(2138350206), Client.packet.ao((byte) 19), 466615625);
                boolean var10000;
                if (Client.packet.aw((byte) 7) == 1) {
                    if (var3 != -1571534834) {
                        throw new IllegalStateException();
                    }

                    var10000 = true;
                } else {
                    var10000 = false;
                }

                boolean var18 = var10000;
                int var7 = Client.packet.readUByte(90109768);
                int var8 = Client.packet.caret * 651432265;
                if (var1.name != null) {
                    if (var3 != -1571534834) {
                        return;
                    }

                    if (var1.cfg != null) {
                        if (var3 != -1571534834) {
                            return;
                        }

                        boolean var9 = false;
                        if (var17.fieldN) {
                            if (var3 != -1571534834) {
                                throw new IllegalStateException();
                            }

                            if (UtilClass1.method260(var1.name, -435132378)) {
                                if (var3 != -1571534834) {
                                    return;
                                }

                                var9 = true;
                            }
                        }

                        if (!var9 && 0 == Client.fieldGs * -1672251869 && !var1.hidden) {
                            String var10;
                            boolean var10001;
                            label228:
                            {
                                Client.fieldAa.caret = 0;
                                Client.packet.read(Client.fieldAa.payload, 0, var7, 1967871236);
                                Client.fieldAa.caret = 0;
                                var10 = classGs.method32(ExchangeOfferTimeComparator.method26(ObjectDefinition.method358(Client.fieldAa, 813726960), -377200329));
                                var1.overheadText = var10.trim();
                                var1.fieldAi = -98561303 * (var4 >> 8);
                                var1.fieldAa = 1482041193 * (var4 & 255);
                                var1.fieldAz = -547011178;
                                var1.fieldAm = var18;
                                if (Renderable.localPlayer != var1 && var17.fieldN && "" != Client.fieldLn) {
                                    if (var10.toLowerCase().indexOf(Client.fieldLn) == -1) {
                                        var10001 = true;
                                        break label228;
                                    }
                                }
                                var10001 = false;
                            }

                            var1.fieldAx = var10001;
                            byte var11;
                            byte var19;
                            if (var17.fieldE) {
                                var19 = (byte) (var18 ? 91 : 1);
                                var11 = var19;
                            } else {
                                if (var18) {
                                    var19 = 90;
                                } else {
                                    var19 = 2;
                                }
                                var11 = var19;
                            }

                            if (-1 != var17.fieldW * 1736764383) {
                                int var14 = var17.fieldW * 1736764383;
                                String var13 = "<img=" + var14 + ">";
                                JagSocket.addMessage(var11, var13 + var1.name, var10, (byte) 16);
                            } else {
                                JagSocket.addMessage(var11, var1.name, var10, (byte) 16);
                            }
                        }
                    }
                }

                Client.packet.caret = -99503879 * (var7 + var8);
            }

            if ((var2 & 2) != 0) {
                var4 = Client.packet.ac((byte) 21);
                if (var4 == '\uffff') {
                    if (var3 != -1571534834) {
                        throw new IllegalStateException();
                    }

                    var4 = -1;
                }

                var16 = Client.packet.readUByte(589557404);
                classX.method203(var1, var4, var16, 1252284258);
            }

        } catch (RuntimeException var15) {
            throw ClanMate.error(var15, "k.bw(" + ')');
        }
    }

    public static CompressedImage method137(ReferenceTable var0, String var1, String var2, int var3) {
        try {
            int var4 = var0.h(var1, 1049523582);
            int var5 = var0.u(var4, var2, -1174997574);
            return classCm.method326(var0, var4, var5, (byte) -88);
        } catch (RuntimeException var6) {
            throw ClanMate.error(var6, "k.m(" + ')');
        }
    }

    static String method138(int var0, byte var1) {
        try {
            if (Client.menuNouns[var0].length() > 0) {
                if (var1 != -13) {
                    throw new IllegalStateException();
                } else {
                    return Client.menuOptions[var0] + StringConstants.fieldFo + Client.menuNouns[var0];
                }
            } else {
                return Client.menuOptions[var0];
            }
        } catch (RuntimeException var2) {
            throw ClanMate.error(var2, "k.ct(" + ')');
        }
    }

    static String method139(Throwable var0, int var1) throws IOException {
        try {
            String var2;
            if (var0 instanceof RSException) {
                if (var1 != -1069846553) {
                    throw new IllegalStateException();
                }

                RSException var3 = (RSException) var0;
                var2 = var3.fieldB + " | ";
                var0 = var3.fieldL;
            } else {
                var2 = "";
            }

            StringWriter var14 = new StringWriter();
            PrintWriter var4 = new PrintWriter(var14);
            var0.printStackTrace(var4);
            var4.close();
            String var5 = var14.toString();
            BufferedReader var6 = new BufferedReader(new StringReader(var5));
            String var7 = var6.readLine();

            while (true) {
                String var8 = var6.readLine();
                if (null == var8) {
                    if (var1 != -1069846553) {
                        throw new IllegalStateException();
                    }

                    var2 = var2 + "| " + var7;
                    return var2;
                }

                int var9 = var8.indexOf(40);
                int var10 = var8.indexOf(41, 1 + var9);
                if (var9 >= 0) {
                    if (var1 != -1069846553) {
                        throw new IllegalStateException();
                    }

                    if (var10 >= 0) {
                        if (var1 != -1069846553) {
                            throw new IllegalStateException();
                        }

                        String var11 = var8.substring(1 + var9, var10);
                        int var12 = var11.indexOf(".java:");
                        if (var12 >= 0) {
                            var11 = var11.substring(0, var12) + var11.substring(5 + var12);
                            var2 = var2 + var11 + ' ';
                            continue;
                        }

                        var8 = var8.substring(0, var9);
                    }
                }

                var8 = var8.trim();
                var8 = var8.substring(var8.lastIndexOf(32) + 1);
                var8 = var8.substring(var8.lastIndexOf(9) + 1);
                var2 = var2 + var8 + ' ';
            }
        } catch (RuntimeException var13) {
            throw ClanMate.error(var13, "k.i(" + ')');
        }
    }

    @Override
    protected final Model getModel(int var1) {
        try {
            if (null != this.fieldW) {
                if (var1 >= -1519654467) {
                    throw new IllegalStateException();
                }

                int var2 = Client.engineCycle * -1223223371 - this.fieldN * 617905529;
                if (var2 > 100) {
                    if (var1 >= -1519654467) {
                        throw new IllegalStateException();
                    }

                    if (this.fieldW.fieldS * -1307272215 > 0) {
                        if (var1 >= -1519654467) {
                            throw new IllegalStateException();
                        }

                        var2 = 100;
                    }
                }

                while (var2 > this.fieldW.fieldE[this.fieldE * 1446706433]) {
                    if (var1 >= -1519654467) {
                        throw new IllegalStateException();
                    }

                    var2 -= this.fieldW.fieldE[this.fieldE * 1446706433];
                    this.fieldE += 1657668353;
                    if (this.fieldE * 1446706433 >= this.fieldW.fieldM.length) {
                        if (var1 >= -1519654467) {
                            throw new IllegalStateException();
                        }

                        this.fieldE -= this.fieldW.fieldS * -1754900759;
                        if (this.fieldE * 1446706433 >= 0) {
                            if (this.fieldE * 1446706433 < this.fieldW.fieldM.length) {
                                continue;
                            }

                            if (var1 >= -1519654467) {
                                throw new IllegalStateException();
                            }
                        }

                        this.fieldW = null;
                        break;
                    }
                }

                this.fieldN = -31302455 * (Client.engineCycle * -1223223371 - var2);
            }

            ObjectDefinition var14 = PlayerConfig.getObjectDef(this.fieldA * 1148047935, 1913332028);


            if (var14.transformIds != null) {
                if (var1 >= -1519654467) {
                    throw new IllegalStateException();
                }

                var14 = var14.transform(1106931628);
            }


           /* if(var14.name.equals("Cogs")) {
                System.out.println("ye");
            }*/


            if (var14 == null) {
                if (var1 >= -1519654467) {
                    throw new IllegalStateException();
                } else {
                    return null;
                }
            } else {
                int var3;
                int var4;
                label77:
                {
                    if (this.fieldI * 463599857 != 1) {
                        if (this.fieldI * 463599857 != 3) {
                            var3 = var14.sizeX * -1151280759;
                            var4 = var14.sizeY * -1255940743;
                            break label77;
                        }

                        if (var1 >= -1519654467) {
                            throw new IllegalStateException();
                        }
                    }

                    var3 = var14.sizeY * -1255940743;
                    var4 = var14.sizeX * -1151280759;
                }

                int var5 = (var3 >> 1) + this.fieldL * -1011699287;
                int var6 = (1 + var3 >> 1) + this.fieldL * -1011699287;
                int var7 = this.fieldM * 1561956911 + (var4 >> 1);
                int var8 = (var4 + 1 >> 1) + this.fieldM * 1561956911;
                int[][] var9 = classM.tileHeights[this.fieldB * -1299151011];
                int var10 = var9[var5][var8] + var9[var6][var7] + var9[var5][var7] + var9[var6][var8] >> 2;
                int var11 = (var3 << 6) + (this.fieldL * -1011699287 << 7);
                int var12 = (this.fieldM * 1561956911 << 7) + (var4 << 6);
                Model m = var14.s(this.fieldV * -527867813, this.fieldI * 463599857, var9, var11, var10, var12, this.fieldW, this.fieldE * 1446706433, 1918502288);

                m.forceColor = true;
                m.color = Color.RED.getRGB();

                return m;
            }
        } catch (RuntimeException var13) {
            throw ClanMate.error(var13, "k.v(" + ')');
        }
    }
}
