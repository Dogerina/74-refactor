public final class UtilClass30 {

    UtilClass30() throws Throwable {
        throw new Error();
    }

    static void method27(int var0, String var1, String var2, String var3, short var4) {
        try {
            MessageContainer var5 = (MessageContainer) UtilClass40.messageContainerMap.get(Integer.valueOf(var0));
            if (var5 == null) {
                if (var4 != 4096) {
                    throw new IllegalStateException();
                }

                var5 = new MessageContainer();
                UtilClass40.messageContainerMap.put(Integer.valueOf(var0), var5);
            }

            Message var6 = var5.a(var0, var1, var2, var3, -205890414);
            UtilClass40.fieldV.v(var6, (long) (var6.fieldA * -1367842983));
            UtilClass40.fieldI.add(var6);
            Client.fieldKd = Client.fieldKe * 261894391;
        } catch (RuntimeException var7) {
            throw ClanMate.error(var7, "gh.i(" + ')');
        }
    }
}
