public class classBu {

    classBk[] fieldV = new classBk[10];
    int fieldI;
    int fieldB;


    classBu(Buffer var1) {
        for (int var2 = 0; var2 < 10; ++var2) {
            int var3 = var1.readUByte(-1205907591);
            if (var3 != 0) {
                var1.caret -= -99503879;
                this.fieldV[var2] = new classBk();
                this.fieldV[var2].i(var1);
            }
        }

        this.fieldI = var1.readUShort(829603351);
        this.fieldB = var1.readUShort(829603351);
    }

    public static classBu method254(ReferenceTable var0, int var1, int var2) {
        byte[] var3 = var0.getFile(var1, var2, (short) 15074);
        return var3 == null ? null : new classBu(new Buffer(var3));
    }

    public final int i() {
        int var1 = 9999999;

        int var2;
        for (var2 = 0; var2 < 10; ++var2) {
            if (this.fieldV[var2] != null && this.fieldV[var2].fieldT / 20 < var1) {
                var1 = this.fieldV[var2].fieldT / 20;
            }
        }

        if (this.fieldI < this.fieldB && this.fieldI / 20 < var1) {
            var1 = this.fieldI / 20;
        }

        if (var1 != 9999999 && var1 != 0) {
            for (var2 = 0; var2 < 10; ++var2) {
                if (this.fieldV[var2] != null) {
                    this.fieldV[var2].fieldT -= var1 * 20;
                }
            }

            if (this.fieldI < this.fieldB) {
                this.fieldI -= var1 * 20;
                this.fieldB -= var1 * 20;
            }

            return var1;
        } else {
            return 0;
        }
    }

    public classBl v() {
        byte[] var1 = this.b();
        return new classBl(22050, var1, this.fieldI * 22050 / 1000, this.fieldB * 22050 / 1000);
    }

    final byte[] b() {
        int var1 = 0;

        int var2;
        for (var2 = 0; var2 < 10; ++var2) {
            if (this.fieldV[var2] != null && this.fieldV[var2].fieldQ + this.fieldV[var2].fieldT > var1) {
                var1 = this.fieldV[var2].fieldQ + this.fieldV[var2].fieldT;
            }
        }

        if (var1 == 0) {
            return new byte[0];
        } else {
            var2 = 22050 * var1 / 1000;
            byte[] var3 = new byte[var2];

            for (int var4 = 0; var4 < 10; ++var4) {
                if (this.fieldV[var4] != null) {
                    int var5 = this.fieldV[var4].fieldQ * 22050 / 1000;
                    int var6 = this.fieldV[var4].fieldT * 22050 / 1000;
                    int[] var7 = this.fieldV[var4].a(var5, this.fieldV[var4].fieldQ);

                    for (int var8 = 0; var8 < var5; ++var8) {
                        int var9 = var3[var8 + var6] + (var7[var8] >> 8);
                        if ((var9 + 128 & -256) != 0) {
                            var9 = var9 >> 31 ^ 127;
                        }

                        var3[var8 + var6] = (byte) var9;
                    }
                }
            }

            return var3;
        }
    }
}
