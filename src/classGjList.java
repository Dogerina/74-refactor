public class classGjList {

    classGj fieldV;
    classGj fieldA = new classGj();


    public classGjList() {
        this.fieldA.fieldB = this.fieldA;
        this.fieldA.fieldL = this.fieldA;
    }

    public classGj v() {
        classGj var1 = this.fieldA.fieldB;
        if (var1 == this.fieldA) {
            this.fieldV = null;
            return null;
        } else {
            this.fieldV = var1.fieldB;
            return var1;
        }
    }

    public void a(classGj var1) {
        if (var1.fieldL != null) {
            var1.delete();
        }

        var1.fieldL = this.fieldA.fieldL;
        var1.fieldB = this.fieldA;
        var1.fieldL.fieldB = var1;
        var1.fieldB.fieldL = var1;
    }

    public classGj i() {
        classGj var1 = this.fieldV;
        if (var1 == this.fieldA) {
            this.fieldV = null;
            return null;
        } else {
            this.fieldV = var1.fieldB;
            return var1;
        }
    }
}
