public class WidgetNode extends Node {

    static int[] fieldDq;
    static classEd fieldOt;
    int type;
    boolean fieldI = false;
    int owner;

    static final void processAction (
            int arg1, int arg2, int opcode, int arg0, String action, String target,
            int mouseX, int mouseY, byte DUMMY
    ) {
        try {
            if(opcode == 3) {
                int id = arg0 >> 14 & 0x7fff;
                System.out.println(id);
            }
            System.out.println("[doAction] Op: " + opcode + ", Arg1: " + arg1 + ", Arg2: " + arg2 + ", Arg0: " + arg0 +
                    ", Action: " + action + ", Target: " + target + ", var6: " + mouseX + ", var7: " + mouseY);
            if (opcode >= 2000) {
                opcode -= 2000;
            }

            if (31 == opcode) {
                Client.fieldCo.writePacketHeader(163, (byte) 0);
                Client.fieldCo.af(arg0, -1285972814);
                Client.fieldCo.writeInt(arg2, 536293685);
                Client.fieldCo.af(Character.fieldIz * 1341679985, -1051063408);
                Client.fieldCo.writeShort(arg1, 1527729208);
                Client.fieldCo.bs(UtilClass6.selectedItemWidgetId * 1795747025, -1315614368);
                Client.fieldCo.aj(UtilClass41.selectedItemIndex * 1003817045, 2055656355);
                Client.fieldGw = 0;
                ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                Client.fieldGr = arg1 * -959053341;
            }

            byte var10001;
            if (opcode == 2) {
                if (DUMMY >= 4) {
                    return;
                }

                Client.fieldGq = 902680581 * mouseX;
                Client.fieldGh = mouseY * 1887175681;
                Client.cursorState = 1976884746;
                Client.fieldGy = 0;
                Client.destinationX = arg1 * -950792229;
                Client.destinationY = -475172701 * arg2;
                Client.fieldCo.writePacketHeader(184, (byte) 0);
                Client.fieldCo.aj(arg0 >> 14 & 32767, 2009058839);
                Client.fieldCo.aj(Client.selectedSpellChildIndex * 891259469, 1982637267);
                Client.fieldCo.ak(classDc.fieldCu * -2071335905 + arg1, 1078918573);
                if (classDx.fieldCd[82]) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var10001 = 1;
                } else {
                    var10001 = 0;
                }

                Client.fieldCo.ai(var10001, 1984668915);
                Client.fieldCo.aj(arg2 + CalendarUtil.fieldCa * -1046564359, 1905702726);
                Client.fieldCo.bs(classG.selectedSpellParentUID * 763146509, -1315614368);
            }

            if (opcode == 33) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldCo.writePacketHeader(32, (byte) 0);
                Client.fieldCo.writeShort(arg1, 1983041820);
                Client.fieldCo.af(arg0, -752606852);
                Client.fieldCo.bi(arg2, (byte) -94);
                Client.fieldGw = 0;
                ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                Client.fieldGr = -959053341 * arg1;
            }

            if (42 == opcode) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldCo.writePacketHeader(61, (byte) 0);
                Client.fieldCo.bi(arg2, (byte) -106);
                Client.fieldCo.aj(arg0, 2123384502);
                Client.fieldCo.aj(arg1, 1933616460);
                Client.fieldGw = 0;
                ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                Client.fieldGr = arg1 * -959053341;
            }

            Npc var9;
            if (opcode == 7) {
                if (DUMMY >= 4) {
                    return;
                }

                var9 = Client.npcs[arg0];
                if (var9 != null) {
                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = -950792229 * arg1;
                    Client.destinationY = -475172701 * arg2;
                    Client.fieldCo.writePacketHeader(225, (byte) 0);
                    Client.fieldCo.aj(UtilClass41.selectedItemIndex * 1003817045, 1859043163);
                    Client.fieldCo.writeInt(UtilClass6.selectedItemWidgetId * 1795747025, -159687963);
                    Client.fieldCo.ak(arg0, 1078918573);
                    if (classDx.fieldCd[82]) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        var10001 = 1;
                    } else {
                        var10001 = 0;
                    }

                    Client.fieldCo.ai(var10001, 241645089);
                    Client.fieldCo.ak(Character.fieldIz * 1341679985, 1078918573);
                }
            }

            if (11 == opcode) {
                var9 = Client.npcs[arg0];
                if (null != var9) {
                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = -950792229 * arg1;
                    Client.destinationY = arg2 * -475172701;
                    Client.fieldCo.writePacketHeader(65, (byte) 0);
                    Client.fieldCo.aj(arg0, 1914986864);
                    Client.fieldCo.a(classDx.fieldCd[82] ? 1 : 0, 2072226620);
                }
            }

            if (opcode == 17) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldGq = mouseX * 902680581;
                Client.fieldGh = 1887175681 * mouseY;
                Client.cursorState = 1976884746;
                Client.fieldGy = 0;
                Client.destinationX = -950792229 * arg1;
                Client.destinationY = arg2 * -475172701;
                Client.fieldCo.writePacketHeader(204, (byte) 0);
                Client.fieldCo.aj(arg0, 1852895451);
                Client.fieldCo.ak(Client.selectedSpellChildIndex * 891259469, 1078918573);
                Client.fieldCo.writeShort(classDc.fieldCu * -2071335905 + arg1, 1548147820);
                Client.fieldCo.writeInvertedByte(classDx.fieldCd[82] ? 1 : 0, (byte) 1);
                Client.fieldCo.aj(arg2 + CalendarUtil.fieldCa * -1046564359, 1937409312);
                Client.fieldCo.bi(classG.selectedSpellParentUID * 763146509, (byte) -27);
            }

            if (opcode == 16) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldGq = 902680581 * mouseX;
                Client.fieldGh = mouseY * 1887175681;
                Client.cursorState = 1976884746;
                Client.fieldGy = 0;
                Client.destinationX = arg1 * -950792229;
                Client.destinationY = -475172701 * arg2;
                Client.fieldCo.writePacketHeader(197, (byte) 0);
                if (classDx.fieldCd[82]) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var10001 = 1;
                } else {
                    var10001 = 0;
                }

                Client.fieldCo.a(var10001, 1033563934);
                Client.fieldCo.bs(UtilClass6.selectedItemWidgetId * 1795747025, -1315614368);
                Client.fieldCo.writeShort(UtilClass41.selectedItemIndex * 1003817045, 591935392);
                Client.fieldCo.writeShort(CalendarUtil.fieldCa * -1046564359 + arg2, 1827742176);
                Client.fieldCo.writeShort(classDc.fieldCu * -2071335905 + arg1, 1340886282);
                Client.fieldCo.ak(arg0, 1078918573);
                Client.fieldCo.af(Character.fieldIz * 1341679985, 2011365406);
            }

            if (opcode == 26) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                World.method181((short) 256);
            }

            if (opcode == 43) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldCo.writePacketHeader(103, (byte) 0);
                Client.fieldCo.af(arg1, 970114918);
                Client.fieldCo.writeInt(arg2, 988159364);
                Client.fieldCo.aj(arg0, 2136493496);
                Client.fieldGw = 0;
                ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                Client.fieldGr = -959053341 * arg1;
            }

            if (18 == opcode) {
                if (DUMMY >= 4) {
                    return;
                }

                Client.fieldGq = mouseX * 902680581;
                Client.fieldGh = mouseY * 1887175681;
                Client.cursorState = 1976884746;
                Client.fieldGy = 0;
                Client.destinationX = arg1 * -950792229;
                Client.destinationY = -475172701 * arg2;
                Client.fieldCo.writePacketHeader(212, (byte) 0);
                if (classDx.fieldCd[82]) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var10001 = 1;
                } else {
                    var10001 = 0;
                }

                Client.fieldCo.writeInvertedByte(var10001, (byte) 53);
                Client.fieldCo.ak(arg1 + classDc.fieldCu * -2071335905, 1078918573);
                Client.fieldCo.af(CalendarUtil.fieldCa * -1046564359 + arg2, 2054469135);
                Client.fieldCo.af(arg0, 1466270543);
            }

            Player var17;
            if (opcode == 51) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                var17 = Client.players[arg0];
                if (var17 != null) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = mouseY * 1887175681;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = arg1 * -950792229;
                    Client.destinationY = -475172701 * arg2;
                    Client.fieldCo.writePacketHeader(38, (byte) 0);
                    Client.fieldCo.ak(arg0, 1078918573);
                    Client.fieldCo.writeInvertedByte(classDx.fieldCd[82] ? 1 : 0, (byte) 46);
                }
            }

            if (30 == opcode) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                if (null == Client.fieldJy) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    UtilClass38.method127(arg2, arg1, (byte) -22);
                    Client.fieldJy = Message.getWidget(arg2, arg1, (byte) 52);
                    classBx.method250(Client.fieldJy, 57574739);
                }
            }

            int var10;
            Widget var18;
            if (opcode == 28) {
                if (DUMMY >= 4) {
                    return;
                }

                Client.fieldCo.writePacketHeader(116, (byte) 0);
                Client.fieldCo.writeInt(arg2, 1902711362);
                var18 = ItemPile.getWidget(arg2, (short) 8250);
                if (var18.fieldDo != null) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    if (5 == var18.fieldDo[0][0]) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        var10 = var18.fieldDo[0][1];
                        UtilClass27.vars[var10] = 1 - UtilClass27.vars[var10];
                        IgnoredPlayer.method94(var10, (short) 22870);
                    }
                }
            }

            if (40 == opcode) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldCo.writePacketHeader(27, (byte) 0);
                Client.fieldCo.af(arg1, 1937323744);
                Client.fieldCo.bo(arg2, 1470065590);
                Client.fieldCo.af(arg0, -2033772155);
                Client.fieldGw = 0;
                ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                Client.fieldGr = -959053341 * arg1;
            }

            if (opcode == 22) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldGq = mouseX * 902680581;
                Client.fieldGh = mouseY * 1887175681;
                Client.cursorState = 1976884746;
                Client.fieldGy = 0;
                Client.destinationX = arg1 * -950792229;
                Client.destinationY = arg2 * -475172701;
                Client.fieldCo.writePacketHeader(240, (byte) 0);
                Client.fieldCo.af(CalendarUtil.fieldCa * -1046564359 + arg2, -1549519603);
                Client.fieldCo.ak(arg1 + classDc.fieldCu * -2071335905, 1078918573);
                Client.fieldCo.ak(arg0, 1078918573);
                if (classDx.fieldCd[82]) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var10001 = 1;
                } else {
                    var10001 = 0;
                }

                Client.fieldCo.ai(var10001, -1122976603);
            }

            if (19 == opcode) {
                if (DUMMY >= 4) {
                    return;
                }

                Client.fieldGq = 902680581 * mouseX;
                Client.fieldGh = 1887175681 * mouseY;
                Client.cursorState = 1976884746;
                Client.fieldGy = 0;
                Client.destinationX = arg1 * -950792229;
                Client.destinationY = -475172701 * arg2;
                Client.fieldCo.writePacketHeader(120, (byte) 0);
                Client.fieldCo.af(classDc.fieldCu * -2071335905 + arg1, -359497695);
                Client.fieldCo.af(CalendarUtil.fieldCa * -1046564359 + arg2, 1119846106);
                Client.fieldCo.ai(classDx.fieldCd[82] ? 1 : 0, 1698069335);
                Client.fieldCo.af(arg0, 1802114637);
            }

            if (36 == opcode) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldCo.writePacketHeader(148, (byte) 0);
                Client.fieldCo.bs(arg2, -1315614368);
                Client.fieldCo.ak(arg0, 1078918573);
                Client.fieldCo.af(arg1, 932826668);
                Client.fieldGw = 0;
                ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                Client.fieldGr = -959053341 * arg1;
            }

            if (opcode == 1004) {
                if (DUMMY >= 4) {
                    return;
                }

                Client.fieldGq = 902680581 * mouseX;
                Client.fieldGh = mouseY * 1887175681;
                Client.cursorState = 1976884746;
                Client.fieldGy = 0;
                Client.fieldCo.writePacketHeader(129, (byte) 0);
                Client.fieldCo.af(arg0, -1425473382);
            }

            if (opcode == 10) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                var9 = Client.npcs[arg0];
                if (var9 != null) {
                    if (DUMMY >= 4) {
                        return;
                    }

                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = -950792229 * arg1;
                    Client.destinationY = -475172701 * arg2;
                    Client.fieldCo.writePacketHeader(19, (byte) 0);
                    Client.fieldCo.aj(arg0, 2139692526);
                    if (classDx.fieldCd[82]) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        var10001 = 1;
                    } else {
                        var10001 = 0;
                    }

                    Client.fieldCo.a(var10001, 2039439204);
                }
            }

            if (opcode == 41) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                }

                Client.fieldCo.writePacketHeader(81, (byte) 0);
                Client.fieldCo.writeInt(arg2, -1084119280);
                Client.fieldCo.af(arg1, 1438696787);
                Client.fieldCo.aj(arg0, 1983462444);
                Client.fieldGw = 0;
                ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                Client.fieldGr = -959053341 * arg1;
            }

            if (opcode == 38) {
                if (DUMMY >= 4) {
                    throw new IllegalStateException();
                } else {
                    classBt.method257((byte) 11);
                    var18 = ItemPile.getWidget(arg2, (short) 8250);
                    Client.itemSelectionStatus = 1688638729;
                    UtilClass41.selectedItemIndex = arg1 * 1496298749;
                    UtilClass6.selectedItemWidgetId = arg2 * 1876618289;
                    Character.fieldIz = -176867439 * arg0;
                    classBx.method250(var18, 57574739);
                    Client.fieldIt = Buffer.method67(16748608, 2096021721) + AnimationSequence.getItemDefinition(arg0, -423577726).fieldX + Buffer.method67(16777215, -1431208449);
                    if (null == Client.fieldIt) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        Client.fieldIt = "null";
                    }

                }
            } else {
                if (45 == opcode) {
                    if (DUMMY >= 4) {
                        return;
                    }

                    var17 = Client.players[arg0];
                    if (null != var17) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        Client.fieldGq = 902680581 * mouseX;
                        Client.fieldGh = 1887175681 * mouseY;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = arg1 * -950792229;
                        Client.destinationY = arg2 * -475172701;
                        Client.fieldCo.writePacketHeader(198, (byte) 0);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.ai(var10001, 1217591831);
                        Client.fieldCo.writeShort(arg0, 339254326);
                    }
                }

                if (opcode == 1001) {
                    if (DUMMY >= 4) {
                        return;
                    }

                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = -950792229 * arg1;
                    Client.destinationY = -475172701 * arg2;
                    Client.fieldCo.writePacketHeader(48, (byte) 0);
                    if (classDx.fieldCd[82]) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        var10001 = 1;
                    } else {
                        var10001 = 0;
                    }

                    Client.fieldCo.writeInvertedByte(var10001, (byte) -9);
                    Client.fieldCo.af(CalendarUtil.fieldCa * -1046564359 + arg2, -1486095796);
                    Client.fieldCo.writeShort(classDc.fieldCu * -2071335905 + arg1, 957241475);
                    Client.fieldCo.writeShort(arg0 >> 14 & 32767, 1390017130);
                }

                if (opcode == 34) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    Client.fieldCo.writePacketHeader(115, (byte) 0);
                    Client.fieldCo.ak(arg1, 1078918573);
                    Client.fieldCo.af(arg0, -1620317985);
                    Client.fieldCo.writeInt(arg2, 1223496312);
                    Client.fieldGw = 0;
                    ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                    Client.fieldGr = arg1 * -959053341;
                }

                if (opcode == 39) {
                    Client.fieldCo.writePacketHeader(123, (byte) 0);
                    Client.fieldCo.bo(arg2, 1449179961);
                    Client.fieldCo.af(arg1, -537662545);
                    Client.fieldCo.writeShort(arg0, 973931455);
                    Client.fieldGw = 0;
                    ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                    Client.fieldGr = arg1 * -959053341;
                }

                if (opcode == 29) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    Client.fieldCo.writePacketHeader(116, (byte) 0);
                    Client.fieldCo.writeInt(arg2, 1846403391);
                    var18 = ItemPile.getWidget(arg2, (short) 8250);
                    if (var18.fieldDo != null) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        if (5 == var18.fieldDo[0][0]) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            var10 = var18.fieldDo[0][1];
                            if (var18.fieldDh[0] != UtilClass27.vars[var10]) {
                                if (DUMMY >= 4) {
                                    throw new IllegalStateException();
                                }

                                UtilClass27.vars[var10] = var18.fieldDh[0];
                                IgnoredPlayer.method94(var10, (short) 15846);
                            }
                        }
                    }
                }

                if (opcode == 1003) {
                    if (DUMMY >= 4) {
                        return;
                    }

                    Client.fieldGq = 902680581 * mouseX;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    var9 = Client.npcs[arg0];
                    if (var9 != null) {
                        NpcDefinition var19 = var9.definition;
                        if (null != var19.transformIds) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            var19 = var19.transform(623536953);
                        }

                        if (var19 != null) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            Client.fieldCo.writePacketHeader(192, (byte) 0);
                            Client.fieldCo.af(var19.fieldL * 753371259, 2022401640);
                        }
                    }
                }

                if (opcode == 3) {
                    Client.fieldGq = 902680581 * mouseX;
                    Client.fieldGh = mouseY * 1887175681;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = arg1 * -950792229;
                    Client.destinationY = arg2 * -475172701;
                    Client.fieldCo.writePacketHeader(221, (byte) 0);
                    Client.fieldCo.af(classDc.fieldCu * -2071335905 + arg1, -1472621214);
                    Client.fieldCo.af(arg0 >> 14 & 32767, 1649047656);
                    Client.fieldCo.writeShort(CalendarUtil.fieldCa * -1046564359 + arg2, 786341628);
                    Client.fieldCo.aa(classDx.fieldCd[82] ? 1 : 0, (byte) 79);
                }

                if (6 == opcode) {
                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = mouseY * 1887175681;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = -950792229 * arg1;
                    Client.destinationY = arg2 * -475172701;
                    Client.fieldCo.writePacketHeader(94, (byte) 0);
                    Client.fieldCo.af(arg2 + CalendarUtil.fieldCa * -1046564359, -697392983);
                    if (classDx.fieldCd[82]) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        var10001 = 1;
                    } else {
                        var10001 = 0;
                    }

                    Client.fieldCo.aa(var10001, (byte) 61);
                    Client.fieldCo.aj(arg1 + classDc.fieldCu * -2071335905, 2024542319);
                    Client.fieldCo.aj(arg0 >> 14 & 32767, 2144518501);
                }

                if (58 == opcode) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var18 = Message.getWidget(arg2, arg1, (byte) 56);
                    if (null != var18) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        Client.fieldCo.writePacketHeader(124, (byte) 0);
                        Client.fieldCo.ak(arg1, 1078918573);
                        Client.fieldCo.af(var18.itemId * -5424365, -1451084030);
                        Client.fieldCo.aj(Client.selectedSpellChildIndex * 891259469, 2143299269);
                        Client.fieldCo.af(Client.fieldJe * -2095193345, 1520264381);
                        Client.fieldCo.writeInt(arg2, -469025434);
                        Client.fieldCo.bi(classG.selectedSpellParentUID * 763146509, (byte) -76);
                    }
                }

                if (23 == opcode) {
                    if (DUMMY >= 4) {
                        return;
                    }

                    UtilClass28.landscape.ai(ClanMate.floorLevel * 87713183, arg1, arg2);
                }

                if (21 == opcode) {
                    if (DUMMY >= 4) {
                        return;
                    }

                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = arg1 * -950792229;
                    Client.destinationY = arg2 * -475172701;
                    Client.fieldCo.writePacketHeader(112, (byte) 0);
                    Client.fieldCo.writeShort(CalendarUtil.fieldCa * -1046564359 + arg2, 580659912);
                    Client.fieldCo.aj(arg0, 2106978805);
                    Client.fieldCo.ak(arg1 + classDc.fieldCu * -2071335905, 1078918573);
                    if (classDx.fieldCd[82]) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        var10001 = 1;
                    } else {
                        var10001 = 0;
                    }

                    Client.fieldCo.writeInvertedByte(var10001, (byte) -2);
                }

                if (12 == opcode) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var9 = Client.npcs[arg0];
                    if (null != var9) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        Client.fieldGq = mouseX * 902680581;
                        Client.fieldGh = mouseY * 1887175681;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = -950792229 * arg1;
                        Client.destinationY = arg2 * -475172701;
                        Client.fieldCo.writePacketHeader(68, (byte) 0);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.writeInvertedByte(var10001, (byte) -89);
                        Client.fieldCo.af(arg0, 731293145);
                    }
                }

                if (opcode == 14) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var17 = Client.players[arg0];
                    if (null != var17) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        Client.fieldGq = mouseX * 902680581;
                        Client.fieldGh = 1887175681 * mouseY;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = arg1 * -950792229;
                        Client.destinationY = arg2 * -475172701;
                        Client.fieldCo.writePacketHeader(165, (byte) 0);
                        Client.fieldCo.aj(Character.fieldIz * 1341679985, 2058143151);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.writeInvertedByte(var10001, (byte) 93);
                        Client.fieldCo.bo(UtilClass6.selectedItemWidgetId * 1795747025, 1161619609);
                        Client.fieldCo.af(UtilClass41.selectedItemIndex * 1003817045, 232632604);
                        Client.fieldCo.ak(arg0, 1078918573);
                    }
                }

                if (opcode == 4) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    Client.fieldGq = 902680581 * mouseX;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = arg1 * -950792229;
                    Client.destinationY = arg2 * -475172701;
                    Client.fieldCo.writePacketHeader(31, (byte) 0);
                    Client.fieldCo.aj(classDc.fieldCu * -2071335905 + arg1, 1939867138);
                    Client.fieldCo.writeShort(arg0 >> 14 & 32767, 598445670);
                    Client.fieldCo.af(arg2 + CalendarUtil.fieldCa * -1046564359, -2130391332);
                    Client.fieldCo.writeInvertedByte(classDx.fieldCd[82] ? 1 : 0, (byte) -10);
                }

                if (1002 == opcode) {
                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.fieldCo.writePacketHeader(133, (byte) 0);
                    Client.fieldCo.af(arg0 >> 14 & 32767, 27913434);
                }

                if (50 == opcode) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var17 = Client.players[arg0];
                    if (null != var17) {
                        Client.fieldGq = 902680581 * mouseX;
                        Client.fieldGh = mouseY * 1887175681;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = arg1 * -950792229;
                        Client.destinationY = -475172701 * arg2;
                        Client.fieldCo.writePacketHeader(69, (byte) 0);
                        Client.fieldCo.ak(arg0, 1078918573);
                        Client.fieldCo.ai(classDx.fieldCd[82] ? 1 : 0, 266102471);
                    }
                }

                if (opcode == 20) {
                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = mouseY * 1887175681;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = -950792229 * arg1;
                    Client.destinationY = arg2 * -475172701;
                    Client.fieldCo.writePacketHeader(231, (byte) 0);
                    if (classDx.fieldCd[82]) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        var10001 = 1;
                    } else {
                        var10001 = 0;
                    }

                    Client.fieldCo.ai(var10001, -371433056);
                    Client.fieldCo.ak(CalendarUtil.fieldCa * -1046564359 + arg2, 1078918573);
                    Client.fieldCo.writeShort(arg0, 1484930726);
                    Client.fieldCo.aj(arg1 + classDc.fieldCu * -2071335905, 2098599202);
                }

                if (5 == opcode) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    Client.fieldGq = mouseX * 902680581;
                    Client.fieldGh = 1887175681 * mouseY;
                    Client.cursorState = 1976884746;
                    Client.fieldGy = 0;
                    Client.destinationX = -950792229 * arg1;
                    Client.destinationY = arg2 * -475172701;
                    Client.fieldCo.writePacketHeader(93, (byte) 0);
                    Client.fieldCo.aj(classDc.fieldCu * -2071335905 + arg1, 1925566267);
                    Client.fieldCo.aj(CalendarUtil.fieldCa * -1046564359 + arg2, 2095155303);
                    Client.fieldCo.ak(arg0 >> 14 & 32767, 1078918573);
                    if (classDx.fieldCd[82]) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        var10001 = 1;
                    } else {
                        var10001 = 0;
                    }

                    Client.fieldCo.a(var10001, 937280292);
                }

                if (32 == opcode) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    Client.fieldCo.writePacketHeader(153, (byte) 0);
                    Client.fieldCo.writeInt(arg2, 960177967);
                    Client.fieldCo.ak(Client.selectedSpellChildIndex * 891259469, 1078918573);
                    Client.fieldCo.ak(arg0, 1078918573);
                    Client.fieldCo.writeInt(classG.selectedSpellParentUID * 763146509, -1113712182);
                    Client.fieldCo.writeShort(arg1, 2008895295);
                    Client.fieldGw = 0;
                    ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                    Client.fieldGr = arg1 * -959053341;
                }

                if (opcode == 49) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var17 = Client.players[arg0];
                    if (var17 != null) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        Client.fieldGq = 902680581 * mouseX;
                        Client.fieldGh = mouseY * 1887175681;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = arg1 * -950792229;
                        Client.destinationY = -475172701 * arg2;
                        Client.fieldCo.writePacketHeader(118, (byte) 0);
                        Client.fieldCo.ak(arg0, 1078918573);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.aa(var10001, (byte) 53);
                    }
                }

                if (opcode == 13) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var9 = Client.npcs[arg0];
                    if (var9 != null) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        Client.fieldGq = mouseX * 902680581;
                        Client.fieldGh = 1887175681 * mouseY;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = -950792229 * arg1;
                        Client.destinationY = arg2 * -475172701;
                        Client.fieldCo.writePacketHeader(181, (byte) 0);
                        Client.fieldCo.af(arg0, -662324714);
                        Client.fieldCo.ai(classDx.fieldCd[82] ? 1 : 0, 1746923780);
                    }
                }

                if (1005 == opcode) {    //Examine Item
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var18 = ItemPile.getWidget(arg2, (short) 8250);
                    if (null != var18 && var18.itemQuantities[arg1] >= 100000) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        JagSocket.addMessage(27, "", var18.itemQuantities[arg1] + " x " + AnimationSequence.getItemDefinition(arg0, 21594233).fieldX, (byte) 16);
                    } else {
                        Client.fieldCo.writePacketHeader(129, (byte) 0);
                        Client.fieldCo.af(arg0, -320546066);
                    }

                    Client.fieldGw = 0;
                    ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                    Client.fieldGr = arg1 * -959053341;
                }

                label1276:
                {
                    if (opcode != 57) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        if (1007 != opcode) {
                            break label1276;
                        }

                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }
                    }

                    var18 = Message.getWidget(arg2, arg1, (byte) 112);
                    if (null != var18) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        var10 = var18.itemId * -5424365;
                        Widget var11 = Message.getWidget(arg2, arg1, (byte) 5);
                        if (var11 == null) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }
                        } else {
                            if (null != var11.fieldCz) {
                                if (DUMMY >= 4) {
                                    return;
                                }

                                ScriptEvent var12 = new ScriptEvent();
                                var12.src = var11;
                                var12.fieldM = arg0 * -683483105;
                                var12.fieldS = target;
                                var12.args = var11.fieldCz;
                                classT.method167(var12, (byte) 1);
                            }

                            boolean var21 = true;
                            if (var11.contentType * 481950881 > 0) {
                                if (DUMMY >= 4) {
                                    return;
                                }

                                var21 = IgnoredPlayer.method95(var11, 892481275);
                            }

                            if (var21) {
                                int var14 = UtilClass38.getWidgetConfig(var11, 2070855251);
                                int var15 = arg0 - 1;
                                boolean var10000;
                                if (0 != (var14 >> 1 + var15 & 1)) {
                                    if (DUMMY >= 4) {
                                        throw new IllegalStateException();
                                    }

                                    var10000 = true;
                                } else {
                                    var10000 = false;
                                }

                                boolean var13 = var10000;
                                if (var13) {
                                    if (1 == arg0) {
                                        if (DUMMY >= 4) {
                                            throw new IllegalStateException();
                                        }

                                        Client.fieldCo.writePacketHeader(237, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, -1592508859);
                                        Client.fieldCo.writeShort(arg1, 216163919);
                                        Client.fieldCo.writeShort(var10, 1240477655);
                                    }

                                    if (arg0 == 2) {
                                        if (DUMMY >= 4) {
                                            return;
                                        }

                                        Client.fieldCo.writePacketHeader(223, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, 916234327);
                                        Client.fieldCo.writeShort(arg1, 791678470);
                                        Client.fieldCo.writeShort(var10, 668898802);
                                    }

                                    if (3 == arg0) {
                                        if (DUMMY >= 4) {
                                            throw new IllegalStateException();
                                        }

                                        Client.fieldCo.writePacketHeader(219, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, -767422016);
                                        Client.fieldCo.writeShort(arg1, 1108570940);
                                        Client.fieldCo.writeShort(var10, 1462132071);
                                    }

                                    if (arg0 == 4) {
                                        if (DUMMY >= 4) {
                                            throw new IllegalStateException();
                                        }

                                        Client.fieldCo.writePacketHeader(188, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, 580241854);
                                        Client.fieldCo.writeShort(arg1, 1599301678);
                                        Client.fieldCo.writeShort(var10, 1988371250);
                                    }

                                    if (arg0 == 5) {
                                        if (DUMMY >= 4) {
                                            throw new IllegalStateException();
                                        }

                                        Client.fieldCo.writePacketHeader(28, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, -1641703606);
                                        Client.fieldCo.writeShort(arg1, 1459385748);
                                        Client.fieldCo.writeShort(var10, 351179450);
                                    }

                                    if (arg0 == 6) {
                                        if (DUMMY >= 4) {
                                            throw new IllegalStateException();
                                        }

                                        Client.fieldCo.writePacketHeader(220, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, -799216190);
                                        Client.fieldCo.writeShort(arg1, 1418046438);
                                        Client.fieldCo.writeShort(var10, 531914216);
                                    }

                                    if (arg0 == 7) {
                                        if (DUMMY >= 4) {
                                            throw new IllegalStateException();
                                        }

                                        Client.fieldCo.writePacketHeader(110, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, -1421775738);
                                        Client.fieldCo.writeShort(arg1, 1780031056);
                                        Client.fieldCo.writeShort(var10, 1552872991);
                                    }

                                    if (8 == arg0) {
                                        Client.fieldCo.writePacketHeader(60, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, 1535312712);
                                        Client.fieldCo.writeShort(arg1, 1782576308);
                                        Client.fieldCo.writeShort(var10, 1376056587);
                                    }

                                    if (9 == arg0) {
                                        if (DUMMY >= 4) {
                                            return;
                                        }

                                        Client.fieldCo.writePacketHeader(146, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, -495600230);
                                        Client.fieldCo.writeShort(arg1, 1885071724);
                                        Client.fieldCo.writeShort(var10, 1108135355);
                                    }

                                    if (arg0 == 10) {
                                        if (DUMMY >= 4) {
                                            throw new IllegalStateException();
                                        }

                                        Client.fieldCo.writePacketHeader(157, (byte) 0);
                                        Client.fieldCo.writeInt(arg2, -2031496860);
                                        Client.fieldCo.writeShort(arg1, 202461277);
                                        Client.fieldCo.writeShort(var10, 1333514381);
                                    }
                                }
                            }
                        }
                    }
                }

                if (9 == opcode) {
                    var9 = Client.npcs[arg0];
                    if (null != var9) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        Client.fieldGq = 902680581 * mouseX;
                        Client.fieldGh = mouseY * 1887175681;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = -950792229 * arg1;
                        Client.destinationY = -475172701 * arg2;
                        Client.fieldCo.writePacketHeader(91, (byte) 0);
                        Client.fieldCo.af(arg0, -971566463);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                return;
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.a(var10001, 95578757);
                    }
                }

                if (opcode == 24) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var18 = ItemPile.getWidget(arg2, (short) 8250);
                    boolean var20 = true;
                    if (var18.contentType * 481950881 > 0) {
                        var20 = IgnoredPlayer.method95(var18, 711560915);
                    }

                    if (var20) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        Client.fieldCo.writePacketHeader(116, (byte) 0);
                        Client.fieldCo.writeInt(arg2, 2068448463);
                    }
                }

                if (37 == opcode) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    Client.fieldCo.writePacketHeader(97, (byte) 0);
                    Client.fieldCo.writeShort(arg0, 1779320366);
                    Client.fieldCo.bs(arg2, -1315614368);
                    Client.fieldCo.ak(arg1, 1078918573);
                    Client.fieldGw = 0;
                    ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                    Client.fieldGr = -959053341 * arg1;
                }

                if (opcode == 8) {
                    if (DUMMY >= 4) {
                        return;
                    }

                    var9 = Client.npcs[arg0];
                    if (var9 != null) {
                        Client.fieldGq = 902680581 * mouseX;
                        Client.fieldGh = mouseY * 1887175681;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = -950792229 * arg1;
                        Client.destinationY = -475172701 * arg2;
                        Client.fieldCo.writePacketHeader(207, (byte) 0);
                        Client.fieldCo.ak(arg0, 1078918573);
                        Client.fieldCo.aj(Client.selectedSpellChildIndex * 891259469, 2016496092);
                        Client.fieldCo.bs(classG.selectedSpellParentUID * 763146509, -1315614368);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                return;
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.ai(var10001, -196960332);
                    }
                }

                if (35 == opcode) {
                    if (DUMMY >= 4) {
                        return;
                    }

                    Client.fieldCo.writePacketHeader(55, (byte) 0);
                    Client.fieldCo.aj(arg0, 2051810788);
                    Client.fieldCo.aj(arg1, 2103679893);
                    Client.fieldCo.writeInt(arg2, -1098964628);
                    Client.fieldGw = 0;
                    ClanMate.fieldGn = ItemPile.getWidget(arg2, (short) 8250);
                    Client.fieldGr = -959053341 * arg1;
                }

                if (44 == opcode) {
                    var17 = Client.players[arg0];
                    if (var17 != null) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        Client.fieldGq = 902680581 * mouseX;
                        Client.fieldGh = 1887175681 * mouseY;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = -950792229 * arg1;
                        Client.destinationY = -475172701 * arg2;
                        Client.fieldCo.writePacketHeader(85, (byte) 0);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                return;
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.aa(var10001, (byte) 93);
                        Client.fieldCo.af(arg0, 2060800904);
                    }
                }

                if (opcode == 47) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    }

                    var17 = Client.players[arg0];
                    if (var17 != null) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        Client.fieldGq = mouseX * 902680581;
                        Client.fieldGh = mouseY * 1887175681;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = -950792229 * arg1;
                        Client.destinationY = -475172701 * arg2;
                        Client.fieldCo.writePacketHeader(189, (byte) 0);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.a(var10001, 1270439587);
                        Client.fieldCo.writeShort(arg0, 968412855);
                    }
                }

                if (25 == opcode) {
                    if (DUMMY >= 4) {
                        throw new IllegalStateException();
                    } else {
                        var18 = Message.getWidget(arg2, arg1, (byte) 39);
                        if (var18 != null) {

                            classBt.method257((byte) -120);
                            int var22 = UtilClass38.getWidgetConfig(var18, 2070855251);
                            int var23 = var22 >> 11 & 63;
                            UtilClass43.method187(arg2, arg1, var23, var18.itemId * -5424365, 353302508);
                            Client.itemSelectionStatus = 0;
                            Client.spellAction = classCs.method304(var18, 469151008);
                            if (Client.spellAction == null) {
                                Client.spellAction = "Null";
                            }

                            if (var18.fieldK) {
                                Client.fieldJc = var18.fieldBa + Buffer.method67(16777215, -2047431907);
                            } else {
                                Client.fieldJc = Buffer.method67('\uff00', 1373289210) + var18.fieldDw + Buffer.method67(16777215, -1062178491);
                            }
                        }

                    }
                } else {
                    if (opcode == 48) {
                        var17 = Client.players[arg0];
                        if (null != var17) {
                            Client.fieldGq = 902680581 * mouseX;
                            Client.fieldGh = mouseY * 1887175681;
                            Client.cursorState = 1976884746;
                            Client.fieldGy = 0;
                            Client.destinationX = -950792229 * arg1;
                            Client.destinationY = -475172701 * arg2;
                            Client.fieldCo.writePacketHeader(132, (byte) 0);
                            Client.fieldCo.aj(arg0, 2088536985);
                            if (classDx.fieldCd[82]) {
                                if (DUMMY >= 4) {
                                    throw new IllegalStateException();
                                }

                                var10001 = 1;
                            } else {
                                var10001 = 0;
                            }

                            Client.fieldCo.writeInvertedByte(var10001, (byte) -74);
                        }
                    }

                    if (opcode == 1) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        Client.fieldGq = mouseX * 902680581;
                        Client.fieldGh = mouseY * 1887175681;
                        Client.cursorState = 1976884746;
                        Client.fieldGy = 0;
                        Client.destinationX = arg1 * -950792229;
                        Client.destinationY = arg2 * -475172701;
                        Client.fieldCo.writePacketHeader(244, (byte) 0);
                        Client.fieldCo.ak(arg0 >> 14 & 32767, 1078918573);
                        Client.fieldCo.aj(arg2 + CalendarUtil.fieldCa * -1046564359, 2111607392);
                        if (classDx.fieldCd[82]) {
                            if (DUMMY >= 4) {
                                return;
                            }

                            var10001 = 1;
                        } else {
                            var10001 = 0;
                        }

                        Client.fieldCo.aa(var10001, (byte) 97);
                        Client.fieldCo.ak(UtilClass41.selectedItemIndex * 1003817045, 1078918573);
                        Client.fieldCo.writeInt(UtilClass6.selectedItemWidgetId * 1795747025, 128529166);
                        Client.fieldCo.af(Character.fieldIz * 1341679985, 997672913);
                        Client.fieldCo.ak(arg1 + classDc.fieldCu * -2071335905, 1078918573);
                    }

                    if (15 == opcode) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        var17 = Client.players[arg0];
                        if (var17 != null) {
                            Client.fieldGq = mouseX * 902680581;
                            Client.fieldGh = mouseY * 1887175681;
                            Client.cursorState = 1976884746;
                            Client.fieldGy = 0;
                            Client.destinationX = arg1 * -950792229;
                            Client.destinationY = arg2 * -475172701;
                            Client.fieldCo.writePacketHeader(71, (byte) 0);
                            Client.fieldCo.writeInt(classG.selectedSpellParentUID * 763146509, -1224801784);
                            Client.fieldCo.af(arg0, -43892136);
                            if (classDx.fieldCd[82]) {
                                if (DUMMY >= 4) {
                                    throw new IllegalStateException();
                                }

                                var10001 = 1;
                            } else {
                                var10001 = 0;
                            }

                            Client.fieldCo.a(var10001, -1227793553);
                            Client.fieldCo.writeShort(Client.selectedSpellChildIndex * 891259469, 1420865302);
                        }
                    }

                    if (opcode == 46) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        var17 = Client.players[arg0];
                        if (null != var17) {
                            if (DUMMY >= 4) {
                                throw new IllegalStateException();
                            }

                            Client.fieldGq = 902680581 * mouseX;
                            Client.fieldGh = mouseY * 1887175681;
                            Client.cursorState = 1976884746;
                            Client.fieldGy = 0;
                            Client.destinationX = arg1 * -950792229;
                            Client.destinationY = arg2 * -475172701;
                            Client.fieldCo.writePacketHeader(82, (byte) 0);
                            if (classDx.fieldCd[82]) {
                                if (DUMMY >= 4) {
                                    throw new IllegalStateException();
                                }

                                var10001 = 1;
                            } else {
                                var10001 = 0;
                            }

                            Client.fieldCo.ai(var10001, 460959200);
                            Client.fieldCo.ak(arg0, 1078918573);
                        }
                    }

                    if (Client.itemSelectionStatus * 279939385 != 0) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        Client.itemSelectionStatus = 0;
                        classBx.method250(ItemPile.getWidget(UtilClass6.selectedItemWidgetId * 1795747025, (short) 8250), 57574739);
                    }

                    if (Client.isSpellSelected) {
                        if (DUMMY >= 4) {
                            throw new IllegalStateException();
                        }

                        classBt.method257((byte) -112);
                    }

                    if (null != ClanMate.fieldGn) {
                        if (DUMMY >= 4) {
                            return;
                        }

                        if (Client.fieldGw * 597120765 == 0) {
                            classBx.method250(ClanMate.fieldGn, 57574739);
                        }
                    }

                }
            }
        } catch (RuntimeException var16) {
            throw ClanMate.error(var16, "b.bq(" + ')');
        }
    }

    static final void method97(int regionX, int regionY, byte DUMMY) {
        try {
            Deque<GroundItem> var3 = Client.groundItemDeques[ClanMate.floorLevel * 87713183][regionX][regionY];
            if (null == var3) {
                if (DUMMY != -2) {
                    throw new IllegalStateException();
                } else {
                    UtilClass28.landscape.removeItemPile(ClanMate.floorLevel * 87713183, regionX, regionY);
                }
            } else {
                long var4 = -99999999L;
                GroundItem mostValuableItem = null;

                GroundItem item;
                for (item = var3.next(); item != null; item = var3.n()) {
                    if (DUMMY != -2) {
                        throw new IllegalStateException();
                    }

                    ItemDefinition var8 = AnimationSequence.getItemDefinition(item.itemId * 2057899465, -126148443);
                    long value = (long) (var8.storeValue * 610558549);
                    if (var8.stackable * 1977514217 == 1) {
                        value *= (long) (1 + item.quantity * 1902127977);
                    }

                    if (value > var4) {
                        if (DUMMY != -2) {
                            throw new IllegalStateException();
                        }

                        var4 = value;
                        mostValuableItem = item;
                    }
                }

                if (mostValuableItem == null) {
                    UtilClass28.landscape.removeItemPile(ClanMate.floorLevel * 87713183, regionX, regionY);
                } else {
                    var3.insert(mostValuableItem);
                    GroundItem var12 = null;
                    GroundItem var13 = null;

                    for (item = var3.next(); null != item; item = var3.n()) {
                        if (item.itemId * 2057899465 != mostValuableItem.itemId * 2057899465) {

                            if (null == var12) {
                                var12 = item;
                            }

                            if (var12.itemId * 2057899465 != item.itemId * 2057899465) {
                                if (var13 == null) {
                                    var13 = item;
                                }
                            }
                        }
                    }

                    int uid = regionX + (regionY << 7) + 1610612736;
                    UtilClass28.landscape.setItemPile(ClanMate.floorLevel * 87713183, regionX, regionY,
                            ItemPile.getTileHeight(regionX * 128 + 64, regionY * 128 + 64, ClanMate.floorLevel * 87713183, 952193188),
                            mostValuableItem, uid, var12, var13);
                }
            }
        } catch (RuntimeException var11) {
            throw ClanMate.error(var11, "b.bs(" + ')');
        }
    }
}
