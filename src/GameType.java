public enum GameType implements Identifiable {

    RUNESCAPE("runescape", "RuneScape", 0),
    STELLARDAWN("stellardawn", "Stellar Dawn", 1),
    GAME3("game3", "Game 3", 2),
    GAME4("game4", "Game 4", 3),
    GAME5("game5", "Game 5", 4),
    OLDSCAPE("oldscape", "RuneScape 2007", 5);


    public final String privateName;
    final int id;


    GameType(String var1, String var2, int var3) {
        this.privateName = var1;
        this.id = var3 * 537275191;
    }

    public static void method188(int var0) {
        try {
            Varpbit.fieldV.clear();
        } catch (RuntimeException var1) {
            throw ClanMate.error(var1, "em.l(" + ')');
        }
    }

    static final void method189(WidgetNode var0, boolean var1, int var2) {
        try {
            int id = var0.owner * -1220935357;
            int key = (int) var0.key;
            var0.delete();
            int var6;
            if (var1) {
                if (-1 != id && Widget.fieldV[id]) {
                    Widget.widgetRefTable.o(id, 1724651399);
                    if (null != Widget.widgets[id]) {
                        boolean var5 = true;
                        for (var6 = 0; var6 < Widget.widgets[id].length; ++var6) {
                            if (Widget.widgets[id][var6] != null) {
                                if (2 != -729517859 * Widget.widgets[id][var6].type) {
                                    Widget.widgets[id][var6] = null;
                                } else {
                                    var5 = false;
                                }
                            }
                        }
                        if (var5) {
                            Widget.widgets[id] = null;
                        }
                        Widget.fieldV[id] = false;
                    }
                }
            }

            for (IntegerNode cfg = (IntegerNode) Client.widgetNodeConfigs.first(); null != cfg; cfg = (IntegerNode) Client.widgetNodeConfigs.next()) {
                if ((cfg.key >> 48 & 65535L) == (long) id) {
                    cfg.delete();
                }
            }

            Widget var9 = ItemPile.getWidget(key, (short) 8250);
            if (null != var9) {
                if (var2 <= -1252244028) {
                    throw new IllegalStateException();
                }

                classBx.method250(var9, 57574739);
            }

            WidgetStrings.method408((byte) -121);
            if (Client.fieldJh * -98490421 != -1) {
                if (var2 <= -1252244028) {
                    throw new IllegalStateException();
                }

                var6 = Client.fieldJh * -98490421;
                if (!AnimationSequence.loadWidget(var6, 477596194)) {
                    if (var2 <= -1252244028) {
                        throw new IllegalStateException();
                    }
                } else {
                    InputHandler.method88(Widget.widgets[var6], 1, 320386219);
                }
            }

        } catch (RuntimeException var7) {
            throw ClanMate.error(var7, "em.dp(" + ')');
        }
    }

    public int getId(int DUMMY) {
        try {
            return this.id * -560891257;
        } catch (RuntimeException var2) {
            throw ClanMate.error(var2, "em.v(" + ')');
        }
    }
}
